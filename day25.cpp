#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

struct Point
{
  Point(int x, int y, int z, int t) : x(x), y(y), z(z), t(t) {}

  int ManhattanDist(const Point & point) const
  {
    return abs(x - point.x) + abs(y - point.y) + abs(z - point.z) + abs(t - point.t);
  }

  bool operator==(const Point & point)
  {
    return this == &point;
  }

  int x;
  int y;
  int z;
  int t;
};

struct Constellation
{
  Constellation(const Point & point)
  {
    points.push_back(point);
  }

  vector<Point> points;
};

int main()
{
  ifstream in("day25.txt");
  ofstream out("out.txt");

  vector<Point> points;
  vector<Constellation> constellations;

  while (!in.eof())
  {
    char c;
    int x, y, z, t;

    in >> x;
    in >> c;
    in >> y;
    in >> c;
    in >> z;
    in >> c;
    in >> t;

    string s;
    std::getline(in, s);

    points.emplace_back(x, y, z, t);
  }

  while (!points.empty())
  {
    bool found = false;

    for (const Point & point : points)
    {
      for (auto & constellation : constellations)
      {
        found = std::any_of(constellation.points.begin(), constellation.points.end(), [&point](const Point & point1)
        {
          return (point.ManhattanDist(point1) <= 3);
        });

        if (found)
        {
          constellation.points.push_back(point);
          auto it = std::find(points.begin(), points.end(), point);
          points.erase(it);
          break;
        }
      }

      if (found)
        break;
    }

    if (!found)
    {
      Constellation newConstellation(points[0]);
      points.erase(points.begin());
      constellations.push_back(newConstellation);
    }
  }

  out << constellations.size() << endl;

  return 0;
}
