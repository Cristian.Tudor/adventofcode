#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <list>
using namespace std;

vector<vector<pair<int, int>>> sortedPaths;
char** a;
int rows = 0, cols = 0, minPath = 1000;
int row[] = { -1, 0, 0, 1 };
int col[] = { 0, -1, 1, 0 };

struct Unit
{
  Unit(int x, int y, bool elf) : x(x), y(y), elf(elf)
  {
    // Part 2 - binary search on elf attack power
  }

  bool operator==(const Unit & unit)
  {
    return x == unit.x && y == unit.y && elf == unit.elf;
  }

  bool IsInRange(const Unit & unit)
  {
    return (x == unit.x && abs(y - unit.y) == 1) || (y == unit.y && abs(x - unit.x) == 1);
  }

  int x;
  int y;
  bool elf;
  int hp{ 200 };
  int attackPower{ 3 };
  bool isDead{ false };
};

bool IsInRange(int srcX, int srcY, const pair<int, int> & dst)
{
  return (srcX == dst.first && abs(srcY - dst.second) == 1) || (srcY == dst.second && abs(srcX - dst.first) == 1);
}

bool IsValid(char ** a, vector<pair<int, int>> & visited, int srcX, int srcY)
{
  if (visited.size() > std::min(minPath, 20))
    return false;

  if (a[srcX][srcY] != '.')
    return false;

  bool found = std::any_of(visited.begin(), visited.end(), [srcX, srcY](const pair<int, int> & cell)
  {
    return srcX == cell.first && srcY == cell.second;
  });

  return !found;
}

void FindAllPaths(int srcX, int srcY, const pair<int, int> & dst, vector<pair<int, int>> & path, vector<pair<int, int>> & visited)
{
  path.push_back(make_pair(srcX, srcY));
  visited.push_back(make_pair(srcX, srcY));

  if (IsInRange(srcX, srcY, dst))
  {
    if (path.size() < minPath)
      minPath = path.size();

    sortedPaths.push_back(path);
  }
  else
  {
    for (int i = 0; i < 4; i++)
    {
      if (IsValid(a, visited, srcX + row[i], srcY + col[i]))
      {
        FindAllPaths(srcX + row[i], srcY + col[i], dst, path, visited);
      }
    }
  }
  
  path.pop_back();
  visited.pop_back();
}

bool readingOrder(const Unit & unit1, const Unit & unit2)
{
  return(unit1.x < unit2.x || (unit1.x == unit2.x && unit1.y < unit2.y));
}

bool hpReadingOrder(const Unit & unit1, const Unit & unit2)
{
  return (unit1.hp < unit2.hp || (unit1.hp == unit2.hp && (unit1.x < unit2.x || (unit1.x == unit2.x && unit1.y < unit2.y))));
}

int main()
{
  ifstream in("day15.txt");
  ofstream out("out.txt");

  while (!in.eof())
  {
    string s;
    in >> s;
    cols = (int)s.size();
    rows++;
  }

  in.clear();
  in.seekg(0);

  a = new char*[rows];
  for (int i = 0; i < rows; i++)
    a[i] = new char[cols];

  vector<Unit> units;
  int row = 0;
  while (!in.eof())
  {
    string s;
    in >> s;

    for (int i = 0; i < s.size(); i++)
    {
      a[row][i] = s[i];
      if (a[row][i] == 'G')
        units.push_back(Unit(row, i, false));
      else if (a[row][i] == 'E')
        units.push_back(Unit(row, i, true));
    }

    row++;
  }

  int roundCount = 0;
  bool combatFinished = false;

  while (!combatFinished)
  {
    std::sort(units.begin(), units.end(), readingOrder);

    for (Unit & unit : units)
    {
      if (unit.isDead)
        continue;

      vector<Unit> enemyUnits;

      for (Unit & unit1 : units)
      {
        if (unit == unit1)
          continue;

        if (unit.elf == unit1.elf)
          continue;

        if (unit.isDead)
          continue;

        enemyUnits.push_back(unit1);
      }

      vector<Unit> enemiesInRange;

      for (Unit & enemy : enemyUnits)
      {
        if (unit.IsInRange(enemy))
          enemiesInRange.push_back(enemy);
      }

      if (enemiesInRange.size() >= 1)
      {
        std::sort(enemiesInRange.begin(), enemiesInRange.end(), hpReadingOrder);

        for (Unit & damagedUnit : units)
        {
          if (damagedUnit == enemiesInRange[0])
          {
            damagedUnit.hp -= unit.attackPower;

            if (damagedUnit.hp <= 0)
            {
              a[damagedUnit.x][damagedUnit.y] = '.';
              damagedUnit.isDead = true;
              
              // Part 2
              // assert(damagedUnit.elf == false);

              bool allElvesDied = std::all_of(units.begin(), units.end(), [](const Unit & unit) 
              {
                if (unit.elf == true)
                  return unit.isDead;

                return true;
              });

              bool allGoblinsDied = std::all_of(units.begin(), units.end(), [](const Unit & unit) 
              {
                if (unit.elf == false)
                  return unit.isDead;

                return true;
              });

              if (allElvesDied || allGoblinsDied)
              {
                combatFinished = true;
                break;
              }
            }
          }
        }

        continue;
      }

      for (Unit & enemy : enemyUnits)
      {
        pair<int, int> dst = make_pair(enemy.x, enemy.y);
        vector<pair<int, int>> path, visited;
        minPath = 1000;

        FindAllPaths(unit.x, unit.y, dst, path, visited);
      }

      if (sortedPaths.empty())
        continue;

      std::sort(sortedPaths.begin(), sortedPaths.end(), [](const vector<pair<int, int>> & path1, const vector<pair<int, int>> & path2)
      {
        if (path1.size() < path2.size())
          return true;

        if (path1.size() > path2.size())
          return false;

        for (int i = 0; i < path1.size(); i++)
        {
          if (path1[i].first < path2[i].first)
            return true;

          if (path1[i].first > path2[i].first)
            return false;
        }

        for (int i = 0; i < path1.size(); i++)
        {
          if (path1[i].second < path2[i].second)
            return true;

          if (path1[i].second > path2[i].second)
            return false;
        }

        return false;
      });

      char val = a[unit.x][unit.y];
      a[unit.x][unit.y] = '.';
      unit.x = sortedPaths[0][1].first;
      unit.y = sortedPaths[0][1].second;
      a[unit.x][unit.y] = val;

      sortedPaths.clear();
    }

    units.erase(std::remove_if(units.begin(), units.end(), [](const Unit & unit) {return unit.isDead; }), units.end());

    if (!combatFinished)
      roundCount++;
  }

  int hpSum = std::accumulate(units.begin(), units.end(), 0, [](int sum, const Unit & unit) {return sum + unit.hp; });

  out << roundCount * (hpSum + 3);
  
  return 0;
}

