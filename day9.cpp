#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <list>
using namespace std;

int main()
{
  ifstream in("day9.txt");
  ofstream out("out.txt");

  int playerCount;
  in >> playerCount;

  string s;
  for (int i = 0; i < 5; i++)
    in >> s;

  int lastMarblePoints;
  in >> lastMarblePoints;

  int marbleCount = lastMarblePoints + 1;

  vector<int> scores;
  scores.resize(playerCount + 1);

  for (int score : scores)
    scores[score] = 0;

  int marbleIndex = 1;
  int playerIndex = 1;
  int currentMarblePos = 0;
  list<int> marbles;
  marbles.push_back(0);
  auto startingIt = marbles.begin();

  while (marbleIndex <= marbleCount)
  {
    if (marbleIndex % 23 != 0)
    {
      if (marbles.size() == 1)
      {
        marbles.push_back(marbleIndex);
        currentMarblePos = 1;
      }
      else
      {
        int index1 = currentMarblePos + 1;
        if (index1 == marbles.size())
          index1 = 0;

        int index2 = index1 + 1;
        if (index2 == marbles.size())
          index2 = 0;

        if (index1 == marbles.size() - 1)
        {
          marbles.push_back(marbleIndex);
          currentMarblePos = marbles.size() - 1;
          startingIt++;
          startingIt++;
        }
        else
        {
          auto it = startingIt;
          if (it != marbles.end())
            it++;

          if (startingIt != marbles.end() && it != marbles.end())
          {
            marbles.insert(it, marbleIndex);
            startingIt = it;
            currentMarblePos = index2;
          }
          else
          {
            it = marbles.begin();
            std::advance(it, index2);
            marbles.insert(it, marbleIndex);
            currentMarblePos = index2;
            startingIt = it;
          }
        }
      }
    }
    else
    {
      scores[playerIndex] += marbleIndex;
      currentMarblePos = currentMarblePos - 7;
      if (currentMarblePos < 0)
      {
        currentMarblePos = marbles.size() + currentMarblePos;
      }

      auto it = marbles.begin();
      std::advance(it, currentMarblePos);

      scores[playerIndex] += *it;
      startingIt = marbles.erase(it);
      startingIt++;
    }

    marbleIndex++;
    if (playerIndex < playerCount)
      playerIndex++;
    else
      playerIndex = 1;
  }

  int maxScore = *std::max_element(scores.begin(), scores.end());

  out << maxScore;

  return 0;
}
