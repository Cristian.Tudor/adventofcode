#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <iterator>
#include <map>
#include <set>
#include <list>
using namespace std;

map<int, set<int>> opcodes;
map<int, set<int>> opcodeRemoveMap;

struct Instruction
{
  Instruction() = default;

  Instruction(int opcode, int a, int b, int c) : opcode(opcode), a(a), b(b), c(c) {}

  int opcode{ 0 };
  int a{ 0 };
  int b{ 0 };
  int c{ 0 };
};

struct RegistersState
{
  RegistersState() = default;

  RegistersState(int a, int b, int c, int d) : A{a}, B{b}, C{c}, D{d} {}

  bool operator==(const RegistersState & regState)
  {
    return A == regState.A && B == regState.B && C == regState.C && D == regState.D;
  }

  int A{ 0 };
  int B{ 0 };
  int C{ 0 };
  int D{ 0 };
};

struct Sample
{
  Sample(const RegistersState & regStateBefore, const Instruction & instr, const RegistersState & regStateAfter)
    : before(regStateBefore), instruction(instr), after(regStateAfter) 
  {}

  RegistersState before;
  Instruction instruction;
  RegistersState after;

  int GetReg(int i) const
  {
    if (i == 0)
      return before.A;

    if (i == 1)
      return before.B;

    if (i == 2)
      return before.C;

    return before.D;
  }

  void SetReg(int i, int val)
  {
    if (i == 0)
      before.A = val;

    else if (i == 1)
      before.B = val;

    else if (i == 2)
      before.C = val;

    else 
      before.D = val;
  }

  bool Matches()
  {
    int count = 0;
    for (int i = 0; i < 16; i++)
    {
      RegistersState beforeTemp(before);
      switch (i)
      {
      case 0:
        SetReg(instruction.c, GetReg(instruction.a) + GetReg(instruction.b));
        break;

      case 1:
        SetReg(instruction.c, GetReg(instruction.a) + instruction.b);
        break;

      case 2:
        SetReg(instruction.c, GetReg(instruction.a) * GetReg(instruction.b));
        break;

      case 3:
        SetReg(instruction.c, GetReg(instruction.a) * instruction.b);
        break;

      case 4:
        SetReg(instruction.c, GetReg(instruction.a) & GetReg(instruction.b));
        break;

      case 5:
        SetReg(instruction.c, GetReg(instruction.a) & instruction.b);
        break;

      case 6:
        SetReg(instruction.c, GetReg(instruction.a) | GetReg(instruction.b));
        break;

      case 7:
        SetReg(instruction.c, GetReg(instruction.a) | instruction.b);
        break;

      case 8:
        SetReg(instruction.c, GetReg(instruction.a));
        break;

      case 9:
        SetReg(instruction.c, instruction.a);
        break;

      case 10:
        SetReg(instruction.c, instruction.a > GetReg(instruction.b) ? 1 : 0);
        break;

      case 11:
        SetReg(instruction.c, GetReg(instruction.a) > instruction.b ? 1 : 0);
        break;

      case 12:
        SetReg(instruction.c, GetReg(instruction.a) > GetReg(instruction.b) ? 1 : 0);
        break;

      case 13:
        SetReg(instruction.c, instruction.a == GetReg(instruction.b) ? 1 : 0);
        break;

      case 14:
        SetReg(instruction.c, GetReg(instruction.a) == instruction.b ? 1 : 0);
        break;

      case 15:
        SetReg(instruction.c, GetReg(instruction.a) == GetReg(instruction.b) ? 1 : 0);
        break;
      }

      if (before == after)
      {
        count++;
        opcodes[instruction.opcode].insert(i);
      }
      else
      {
        auto it = opcodes[instruction.opcode].find(i);
        if (it != opcodes[instruction.opcode].end())
          opcodeRemoveMap[instruction.opcode].insert(i);
      }

      before = beforeTemp;
    }

    return count >= 3;
  }
};

int main()
{
  ifstream in("day16.txt");
  ofstream out("out.txt");

  vector<Sample> samples;
  vector<Instruction> instructions;

  while (!in.eof())
  {
    string temp;
    in >> temp;

    if (temp == "Before:")
    {
      char ch;
      in >> ch;

      int A, B, C, D;
      in >> A;
      in >> ch;
      in >> B;
      in >> ch;
      in >> C;
      in >> ch;
      in >> D;
      std::getline(in, temp);

      RegistersState before(A, B, C, D);

      int opcode, a, b, c;
      in >> opcode;
      in >> a;
      in >> b;
      in >> c;
      std::getline(in, temp);

      Instruction instr(opcode, a, b, c);

      in >> temp;
      in >> ch;

      in >> A;
      in >> ch;
      in >> B;
      in >> ch;
      in >> C;
      in >> ch;
      in >> D;
      std::getline(in, temp);

      RegistersState after(A, B, C, D);

      samples.push_back(Sample(before, instr, after));
    }
    else
    {
      int opcode, a, b, c;
      opcode = std::stoi(temp);
      in >> a;
      in >> b;
      in >> c;

      instructions.push_back(Instruction(opcode, a, b, c));
    }
  }

  int count = count_if(samples.begin(), samples.end(), [](Sample & sample) { return sample.Matches(); });
  out << count << endl;

  for (auto & opcode : opcodes)
  {
    std::set<int> diff;
    std::set_difference(opcode.second.begin(), opcode.second.end(),
      opcodeRemoveMap[opcode.first].begin(), opcodeRemoveMap[opcode.first].end(), std::inserter(diff, diff.begin()));

    opcodes[opcode.first] = diff;
  }

  while (true)
  {
    bool found = false;

    for (const auto & elem : opcodes)
    {
      if (elem.second.size() == 1)
      {
        for (auto & elem1 : opcodes)
        {
          if (elem1 != elem && elem1.second.find(*elem.second.begin()) != elem1.second.end())
          {
            elem1.second.erase(elem1.second.find(*elem.second.begin()));
            found = true;
            break;
          }
        }

        if (found)
          break;
      }
    }

    if (!found)
      break;
  }

  RegistersState before, after;
  Instruction instr;
  Sample sample(before, instr, after);

  for (const Instruction & instruction : instructions)
  {
    switch (*opcodes[instruction.opcode].begin())
    {
    case 0:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) + sample.GetReg(instruction.b));
      break;

    case 1:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) + instruction.b);
      break;

    case 2:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) * sample.GetReg(instruction.b));
      break;

    case 3:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) * instruction.b);
      break;

    case 4:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) & sample.GetReg(instruction.b));
      break;

    case 5:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) & instruction.b);
      break;

    case 6:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) | sample.GetReg(instruction.b));
      break;

    case 7:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) | instruction.b);
      break;

    case 8:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a));
      break;

    case 9:
      sample.SetReg(instruction.c, instruction.a);
      break;

    case 10:
      sample.SetReg(instruction.c, instruction.a > sample.GetReg(instruction.b) ? 1 : 0);
      break;

    case 11:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) > instruction.b ? 1 : 0);
      break;

    case 12:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) > sample.GetReg(instruction.b) ? 1 : 0);
      break;

    case 13:
      sample.SetReg(instruction.c, instruction.a == sample.GetReg(instruction.b) ? 1 : 0);
      break;

    case 14:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) == instruction.b ? 1 : 0);
      break;

    case 15:
      sample.SetReg(instruction.c, sample.GetReg(instruction.a) == sample.GetReg(instruction.b) ? 1 : 0);
      break;
    }
  }

  out << sample.before.A;
  return 0;
}
