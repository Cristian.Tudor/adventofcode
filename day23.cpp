#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <queue>
#include <map>
using namespace std;

struct Nanobot
{
  Nanobot(int x, int y, int z, int radius) : x(x), y(y), z(z), radius(radius) {}

  int x;
  int y;
  int z;
  int radius;
};

int ManhattanDist(const Nanobot & nanobot1, const Nanobot & nanobot2)
{
  return abs(nanobot1.x - nanobot2.x) + abs(nanobot1.y - nanobot2.y) + abs(nanobot1.z - nanobot2.z);
}

void PartOne()
{
  ifstream in("day23.txt");
  ofstream out("out.txt");

  vector<Nanobot> nanobots;

  while (!in.eof())
  {
    char c;
    for (int i = 0; i < 5; i++)
      in >> c;

    int x, y, z;
    in >> x;
    in >> c;
    in >> y;
    in >> c;
    in >> z;

    for (int i = 0; i < 4; i++)
      in >> c;

    int radius;
    in >> radius;

    string temp;
    std::getline(in, temp);

    nanobots.emplace_back(x, y, z, radius);
  }

  auto & maxNanobot = *std::max_element(nanobots.begin(), nanobots.end(), [](const Nanobot & nanobot1, const Nanobot & nanobot2)
  {
    return nanobot1.radius < nanobot2.radius;
  });

  int count = 0;
  for (const Nanobot & nanobot : nanobots)
  {
    if (ManhattanDist(maxNanobot, nanobot) <= maxNanobot.radius)
      count++;
  }

  out << count;
}

void PartTwo()
{
  ifstream in("day23.txt");
  ofstream out("out.txt");

  vector<Nanobot> nanobots;

  while (!in.eof())
  {
    char c;
    for (int i = 0; i < 5; i++)
      in >> c;

    int x, y, z;
    in >> x;
    in >> c;
    in >> y;
    in >> c;
    in >> z;

    for (int i = 0; i < 4; i++)
      in >> c;

    int radius;
    in >> radius;

    string temp;
    std::getline(in, temp);

    nanobots.emplace_back(x, y, z, radius);
  }

  int dist = 1, maxX = 0, maxY = 0, maxZ = 0, minX = INT_MAX, minY = INT_MAX, minZ = INT_MAX;

  for (const Nanobot & nanobot : nanobots)
  {
    if (nanobot.x > maxX)
      maxX = nanobot.x;

    if (nanobot.y > maxY)
      maxY = nanobot.y;

    if (nanobot.z > maxZ)
      maxZ = nanobot.z;

    if (nanobot.x < minX)
      minX = nanobot.x;

    if (nanobot.y < minY)
      minY = nanobot.y;

    if (nanobot.z < minZ)
      minZ = nanobot.z;
  }

  auto & maxNanobot = *std::max_element(nanobots.begin(), nanobots.end(), [](const Nanobot & nanobot1, const Nanobot & nanobot2)
  {
    return nanobot1.radius < nanobot2.radius;
  });

  int maxRadius = maxNanobot.radius;

  while (dist < maxX - minX)
    dist *= 2;

  while (true)
  {
    int targetCount = 0, bestVal = 0;
    Nanobot nanobot(0, 0, 0, 0);

    for (int x = minX; x <= maxX + 1; x += dist)
    {
      for (int y = minY; y <= maxY + 1; y += dist)
      {
        for (int z = minZ; z <= maxZ + 1; z += dist)
        {
          int count = 0;

          for (const Nanobot & bot : nanobots)
          {
            int calc = abs(x - bot.x) + abs(y - bot.y) + abs(z - bot.z);
            if ((calc - maxRadius) / dist <= 0)
              count++;
          }

          if (count > targetCount)
          {
            targetCount = count;
            bestVal = abs(x) + abs(y) + abs(z);
            nanobot.x = x;
            nanobot.y = y;
            nanobot.z = z;
          }
          else if (count == targetCount)
          {
            if (abs(x) + abs(y) + abs(z) < bestVal)
            {
              bestVal = abs(x) + abs(y) + abs(z);
              nanobot.x = x;
              nanobot.y = y;
              nanobot.z = z;
            }
          }
        }
      }
    }

    if (dist == 1)
    {
      out << bestVal << endl;
      break;
    }

    minX = nanobot.x - dist;
    maxX = nanobot.x + dist;
    minY = nanobot.y - dist;
    maxY = nanobot.y + dist;
    minZ = nanobot.z - dist;
    maxZ = nanobot.z + dist;
    dist /= 2;
  }
}

int main()
{
  PartTwo();

  return 0;
}
