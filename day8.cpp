#include <fstream>
#include <numeric>
#include <string>
#include <vector>
using namespace std;

void PartOne();
void PartTwo();

struct Node
{
  Node(int aChildCount, int aMetadataCount)
    : childCount(aChildCount), metadataCount(aMetadataCount)
  {
  }

  int childCount;
  int metadataCount;

  vector<Node *> childNodes;
  vector<int> metadata;
};

Node * rootNode{ nullptr };
int sum = 0, value = 0;

void Traversal(Node * node)
{
  for (Node * node : node->childNodes)
  {
    sum += std::accumulate(node->metadata.begin(), node->metadata.end(), 0);
    Traversal(node);
  }
}

void ValueTraversal(Node * node)
{
  if (node->childCount == 0)
  {
    value += std::accumulate(node->metadata.begin(), node->metadata.end(), 0);
  }
  else
  {
    for (int index : node->metadata)
    {
      if (index == 0)
        continue;

      if (index > node->childCount)
        continue;

      ValueTraversal(node->childNodes[index - 1]);
    }
  }
}

Node * RecursiveAdd(ifstream & in)
{
  int childCount;
  in >> childCount;

  int metadataCount;
  in >> metadataCount;

  Node * newNode = new Node(childCount, metadataCount);
  if (!rootNode)
    rootNode = newNode;

  for (int i = 0; i < childCount; i++)
  {
    Node * childNode = RecursiveAdd(in);
    newNode->childNodes.push_back(childNode);
  }

  for (int i = 0; i < metadataCount; i++)
  {
    int metadata;
    in >> metadata;

    newNode->metadata.push_back(metadata);
  }

  return newNode;
}

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("in.txt");
  ofstream out("out.txt");

  while (!in.eof())
  {
    RecursiveAdd(in);
  }

  sum = std::accumulate(rootNode->metadata.begin(), rootNode->metadata.end(), 0);

  Traversal(rootNode);

  out << sum;
}

void PartTwo()
{
  ifstream in("in.txt");
  ofstream out("out.txt");

  while (!in.eof())
  {
    RecursiveAdd(in);
  }

  ValueTraversal(rootNode);

  out << value;
}
