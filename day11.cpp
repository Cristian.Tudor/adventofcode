#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <list>
using namespace std;

void PartOne();
void PartTwo();

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day11.txt");
  ofstream out("out.txt");

  int gridSerialNumber;
  in >> gridSerialNumber;

  int a[301][301];

  for (int i = 1; i <= 300; i++)
  {
    for (int j = 1; j <= 300; j++)
    {
      int rackId = j + 10;
      int powerLevel = rackId * i;
      int x = powerLevel + gridSerialNumber;
      int y = x * rackId;
      int z = 0;
      if (y >= 100)
        z = (y / 100) % 10;
      a[j][i] = z - 5;
    }
  }

  int maxPower = 0, x = 0, y = 0;
  for (int i = 1; i <= 298; i++)
  {
    for (int j = 1; j <= 298; j++)
    {
      int power = 0;
      for (int k = i; k <= i + 2; k++)
        for (int l = j; l <= j + 2; l++)
          power += a[k][l];

      if (power > maxPower)
      {
        maxPower = power;
        x = i;
        y = j;
      }
    }
  }

  out << x << "," << y;
}

void PartTwo()
{
  ifstream in("day11.txt");
  ofstream out("out.txt");

  int gridSerialNumber;
  in >> gridSerialNumber;

  int a[301][301];

  for (int i = 1; i <= 300; i++)
  {
    for (int j = 1; j <= 300; j++)
    {
      int rackId = j + 10;
      int powerLevel = rackId * i;
      int x = powerLevel + gridSerialNumber;
      int y = x * rackId;
      int z = 0;
      if (y >= 100)
        z = (y / 100) % 10;
      a[j][i] = z - 5;
    }
  }

  int maxPower = 0, x = 0, y = 0, maxSize = 0;
  for (int size = 1; size <= 300; size++)
  {
    for (int i = 1; i <= 300 - size + 1; i++)
    {
      for (int j = 1; j <= 300 - size + 1; j++)
      {
        int power = 0;
        for (int k = i; k <= i + size - 1; k++)
          for (int l = j; l <= j + size - 1; l++)
            power += a[k][l];

        if (power > maxPower)
        {
          maxPower = power;
          maxSize = size;
          x = i;
          y = j;
        }
      }
    }
  }

  out << x << "," << y << "," << maxSize;
}
