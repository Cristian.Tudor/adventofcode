#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <unordered_map>
using namespace std;

struct Data
{
  Data(int month, int day, int hour, int minute, int id, bool beginsShift)
    : mMonth(month), mDay(day), mHour(hour), mMinute(minute), mId(id), mBeginsShift(beginsShift)
  {}

  int mMonth{ 0 };
  int mDay{ 0 };
  int mHour{ 0 };
  int mMinute{ 0 };
  int mId{ 0 };
  bool mBeginsShift{ false };
};

void PartOne();
void PartTwo();

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day4.txt");
  ofstream out("out.txt");

  vector<Data> records;

  while (!in.eof())
  {
    char c;
    in >> c;

    int year;
    in >> year;
    in >> c;

    int month, day;
    in >> month;
    in >> c;
    in >> day;

    int hour, minute;
    in >> hour;
    in >> c;
    in >> minute;
    in >> c;

    string s;
    in >> s;

    if (s == "Guard")
    {
      in >> c;
      int id;
      in >> id;

      string p;
      std::getline(in, p);

      Data data(month, day, hour, minute, id, true);
      records.push_back(std::move(data));
    }
    else
    {
      string p;
      std::getline(in, p);

      Data data(month, day, hour, minute, 0, false);
      records.push_back(std::move(data));
    }
  }

  std::sort(records.begin(), records.end(), [](const Data & data1, const Data & data2)
  {
    return std::tie(data1.mMonth, data1.mDay, data1.mHour, data1.mMinute) < std::tie(data2.mMonth, data2.mDay, data2.mHour, data2.mMinute);
  });

  unordered_map<int, vector<pair<int, int>>> guardRecords;

  size_t i = 0;
  while (i < records.size())
  {
    if (records[i].mBeginsShift)
    {
      size_t j = i + 1;
      while (j < records.size() && !records[j].mBeginsShift)
        j++;

      for (size_t k = i; k < j - 1; k += 2)
      {
        guardRecords[records[i].mId].push_back(make_pair(records[k + 1].mMinute, records[k + 2].mMinute));
      }

      i = j;
    }
  }

  vector<pair<int, int>> guardIdMinutes;

  for (const auto & entry : guardRecords)
  {
    int minuteCount = 0;

    for (auto data : entry.second)
    {
      minuteCount += (data.second - data.first);
    }

    guardIdMinutes.push_back(make_pair(entry.first, minuteCount));
  }

  int max = 0, id = 0;
  for (const auto & entry : guardIdMinutes)
  {
    if (entry.second > max)
    {
      max = entry.second;
      id = entry.first;
    }
  }

  unordered_map<int, int> occCount;
  for (const auto & entry : guardRecords)
  {
    if (entry.first == id)
    {
      for (const auto & val : guardIdMinutes)
      {
        if (val.first == id)
        {
          for (const auto & x : entry.second)
            for (int i = x.first; i < x.second; i++)
              if (any_of(occCount.begin(), occCount.end(), [i](const pair<int, int> & value) { return value.first == i; }))
                occCount[i]++;
              else
                occCount[i] = 1;
        }
      }
    }
  }

  int maxCount = 0;
  int val = 0;
  for (const auto & x : occCount)
  {
    if (x.second > maxCount)
    {
      maxCount = x.second;
      val = x.first;
    }
  }

  out << id * val;
}

void PartTwo()
{
  ifstream in("day4.txt");
  ofstream out("out.txt");

  vector<Data> records;

  while (!in.eof())
  {
    char c;
    in >> c;

    int year;
    in >> year;
    in >> c;

    int month, day;
    in >> month;
    in >> c;
    in >> day;

    int hour, minute;
    in >> hour;
    in >> c;
    in >> minute;
    in >> c;

    string s;
    in >> s;

    if (s == "Guard")
    {
      in >> c;
      int id;
      in >> id;

      string p;
      std::getline(in, p);

      Data data(month, day, hour, minute, id, true);
      records.push_back(std::move(data));
    }
    else
    {
      string p;
      std::getline(in, p);

      Data data(month, day, hour, minute, 0, false);
      records.push_back(std::move(data));
    }
  }

  std::sort(records.begin(), records.end(), [](const Data & data1, const Data & data2)
  {
    return std::tie(data1.mMonth, data1.mDay, data1.mHour, data1.mMinute) < std::tie(data2.mMonth, data2.mDay, data2.mHour, data2.mMinute);
  });

  unordered_map<int, vector<pair<int, int>>> guardRecords;

  size_t i = 0;
  while (i < records.size())
  {
    if (records[i].mBeginsShift)
    {
      size_t j = i + 1;
      while (j < records.size() && !records[j].mBeginsShift)
        j++;

      for (size_t k = i; k < j - 1; k += 2)
      {
        guardRecords[records[i].mId].push_back(make_pair(records[k + 1].mMinute, records[k + 2].mMinute));
      }

      i = j;
    }
  }

  vector<pair<int, int>> guardIdMinutes;

  for (const auto & entry : guardRecords)
  {
    int minuteCount = 0;

    for (auto data : entry.second)
    {
      minuteCount += (data.second - data.first);
    }

    guardIdMinutes.push_back(make_pair(entry.first, minuteCount));
  }

  int max = 0, id = 0;
  for (const auto & entry : guardIdMinutes)
  {
    if (entry.second > max)
    {
      max = entry.second;
      id = entry.first;
    }
  }

  unordered_map<int, unordered_map<int, int>> occCount;
  for (const auto & entry : guardRecords)
  {
    for (const auto & x : entry.second)
      for (int i = x.first; i < x.second; i++)
      {
        int id = entry.first;

        if (any_of(occCount.begin(), occCount.end(), [id, i](const pair<int, unordered_map<int, int>> & value)
        {
          return id == value.first && any_of(value.second.begin(), value.second.end(), [i](auto & myPair) { return myPair.first == i; });
        }))
          occCount[entry.first][i]++;
        else
          occCount[entry.first][i] = 1;
      }
  }

  int newId = 0;
  int minute = 0;
  max = 0;

  for (const auto & entry1 : occCount)
  {
    for (const auto & perEntryPairs : entry1.second)
    {
      if (perEntryPairs.second > max)
      {
        max = perEntryPairs.second;
        newId = entry1.first;
        minute = perEntryPairs.first;
      }
    }
  }

  out << newId * minute;
}
