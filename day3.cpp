#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

void PartOne();
void PartTwo();

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day3.txt");
  ofstream out("out.txt");

  int rows = 1000, cols = 1000;
  int** a = new int*[rows];
  for (int i = 0; i < rows; i++)
    a[i] = new int[cols];

  for (int i = 0; i < 1000; i++)
    for (int j = 0; j < 1000; j++)
      a[i][j] = 0;

  int overlap = 0;
  vector<pair<int, int>> overlapPairs;

  while (!in.eof())
  {
    string s;
    std::getline(in, s);

    size_t spacePos = s.find(' ');
    int id = std::stoi(s.substr(1, spacePos - 1));

    size_t edgePos = s.find("@ ") + 2;
    size_t commaPos = s.find(',');
    size_t colonPos = s.find(':');
    size_t multiplyPos = s.find('x');
    
    int leftOffset = std::stoi(s.substr(edgePos, commaPos - edgePos));
    int topOffset = std::stoi(s.substr(commaPos + 1, colonPos - commaPos - 1));
    int width = std::stoi(s.substr(colonPos + 2, multiplyPos - colonPos - 2));
    int height = std::stoi(s.substr(multiplyPos + 1));

    for (int i = topOffset; i < topOffset + height; i++)
      for (int j = leftOffset; j < leftOffset + width; j++)
        if (a[i][j] != 0)
        {
          if (!std::any_of(overlapPairs.begin(), overlapPairs.end(), [i, j](pair<int, int> & val) {return val.first == i && val.second == j; }))
          {
            overlap++;
            overlapPairs.push_back(make_pair(i, j));
          }
        }
        else
          a[i][j] = id;
  }

  out << overlap;

  for (int i = 0; i < rows; ++i)
    delete[] a[i];
  delete[] a;
}

void PartTwo()
{
  ifstream in("day3.txt");
  ofstream out("out.txt");

  int rows = 1000, cols = 1000;
  int** a = new int*[rows];
  for (int i = 0; i < rows; i++)
    a[i] = new int[cols];

  for (int i = 0; i < 1000; i++)
    for (int j = 0; j < 1000; j++)
      a[i][j] = 0;

  vector<pair<int, int>> overlapPairs;

  while (!in.eof())
  {
    string s;
    std::getline(in, s);

    size_t spacePos = s.find(' ');
    int id = std::stoi(s.substr(1, spacePos - 1));

    size_t edgePos = s.find("@ ") + 2;
    size_t commaPos = s.find(',');
    size_t colonPos = s.find(':');
    size_t multiplyPos = s.find('x');

    int leftOffset = std::stoi(s.substr(edgePos, commaPos - edgePos));
    int topOffset = std::stoi(s.substr(commaPos + 1, colonPos - commaPos - 1));
    int width = std::stoi(s.substr(colonPos + 2, multiplyPos - colonPos - 2));
    int height = std::stoi(s.substr(multiplyPos + 1));

    for (int i = topOffset; i < topOffset + height; i++)
      for (int j = leftOffset; j < leftOffset + width; j++)
        if (a[i][j] != 0)
        {
          if (!std::any_of(overlapPairs.begin(), overlapPairs.end(), [i, j](pair<int, int> & val) {return val.first == i && val.second == j; }))
            overlapPairs.push_back(make_pair(i, j));
        }
        else
          a[i][j] = id;
  }

  in.clear();
  in.seekg(0);

  while (!in.eof())
  {
    string s;
    std::getline(in, s);

    size_t spacePos = s.find(' ');
    int id = std::stoi(s.substr(1, spacePos - 1));

    size_t edgePos = s.find("@ ") + 2;
    size_t commaPos = s.find(',');
    size_t colonPos = s.find(':');
    size_t multiplyPos = s.find('x');

    int leftOffset = std::stoi(s.substr(edgePos, commaPos - edgePos));
    int topOffset = std::stoi(s.substr(commaPos + 1, colonPos - commaPos - 1));
    int width = std::stoi(s.substr(colonPos + 2, multiplyPos - colonPos - 2));
    int height = std::stoi(s.substr(multiplyPos + 1));

    bool found = false;

    for (int i = topOffset; i < topOffset + height; i++)
      for (int j = leftOffset; j < leftOffset + width; j++)
        if (std::any_of(overlapPairs.begin(), overlapPairs.end(), [i, j](pair<int, int> & val) {return val.first == i && val.second == j; }))
          found = true;

    if (!found)
      out << id;
  }

  for (int i = 0; i < rows; ++i)
    delete[] a[i];
  delete[] a;
}
