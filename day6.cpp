#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <map>
#include <cmath>
#include <assert.h>
using namespace std;

void PartOne();
void PartTwo();

int ManhattanDist(int x1, int y1, int x2, int y2)
{
  return abs(x2 - x1) + abs(y2 - y1);
}

vector<pair<int, int>> v1;

int recursive_sum(vector<pair<int, int>> & visited, int i, int j, int rows, int cols)
{
  int sum = 0;

  if (i > 0 && !std::any_of(visited.begin(), visited.end(), [i, j](const pair<int, int> & x) {return x.first == i - 1 && x.second == j; }))
  {
    bool found = std::any_of(v1.begin(), v1.end(), [i, j](const pair<int, int> & p) {return p.first == i - 1 && p.second == j; });
    if (found)
    {
      visited.push_back(make_pair(i - 1, j));
      sum += recursive_sum(visited, i - 1, j, rows, cols) + 1;
    }
  }

  if (i < rows - 1 && !std::any_of(visited.begin(), visited.end(), [i, j](const pair<int, int> & x) {return x.first == i + 1 && x.second == j; }))
  {
    bool found = std::any_of(v1.begin(), v1.end(), [i, j](const pair<int, int> & p) {return p.first == i + 1 && p.second == j; });
    if (found)
    {
      visited.push_back(make_pair(i + 1, j));
      sum += recursive_sum(visited, i + 1, j, rows, cols) + 1;
    }
  }

  if (j > 0 && !std::any_of(visited.begin(), visited.end(), [i, j](const pair<int, int> & x) {return x.first == i && x.second == j - 1; }))
  {
    bool found = std::any_of(v1.begin(), v1.end(), [i, j](const pair<int, int> & p) {return p.first == i && p.second == j - 1; });
    if (found)
    {
      visited.push_back(make_pair(i, j - 1));
      sum += recursive_sum(visited, i, j - 1, rows, cols) + 1;
    }
  }

  if (j < cols - 1 && !std::any_of(visited.begin(), visited.end(), [i, j](const pair<int, int> & x) {return x.first == i && x.second == j+1; }))
  {
    bool found = std::any_of(v1.begin(), v1.end(), [i, j](const pair<int, int> & p) {return p.first == i && p.second == j + 1; });
    if (found)
    {
      visited.push_back(make_pair(i, j + 1));
      sum += recursive_sum(visited, i, j + 1, rows, cols) + 1;
    }
  }

  return sum;
}

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day6.txt");
  ofstream out("out.txt");

  vector<int> xCoord;
  vector<int> yCoord;

  while (!in.eof())
  {
    char c;
    int x, y;
    in >> y;
    in >> c;
    in >> x;

    xCoord.push_back(x);
    yCoord.push_back(y);
  }

  int maxX = *std::max_element(xCoord.begin(), xCoord.end()) + 1;
  int maxY = *std::max_element(yCoord.begin(), yCoord.end()) + 1;

  int rows = maxX > maxY ? maxX : maxY;
  int cols = rows;

  int** a = new int*[rows];
  for (int i = 0; i < rows; i++)
    a[i] = new int[cols];

  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      a[i][j] = 0;

  in.clear();
  in.seekg(0);
  int nr = 1;

  vector<pair<int, int>> v;

  while (!in.eof())
  {
    char c;
    int x, y;
    in >> y;
    in >> c;
    in >> x;

    a[x][y] = nr;
    v.push_back(make_pair(x, y));
    nr += 2;
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      bool found = std::any_of(v.begin(), v.end(), [i, j](const pair<int, int> & p) {return p.first == i && p.second == j; });
      if (found)
        continue;

      int minDist = 100000;
      int count = 0;
      int x1 = 0, y1 = 0;
      for (pair<int, int> & p : v)
      {
        if (ManhattanDist(i, j, p.first, p.second) < minDist)
        {
          minDist = ManhattanDist(i, j, p.first, p.second);
          count = 1;
          x1 = p.first;
          y1 = p.second;
        }
        else if (ManhattanDist(i, j, p.first, p.second) == minDist)
        {
          count++;
        }
      }

      if (count == 1)
        a[i][j] = a[x1][y1] + 1;
      else
        a[i][j] = -1;
    }
  }

  map<int, int> occCount;
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      if (a[i][j] % 2 == 0)
        occCount[a[i][j]] = 0;
      else
        occCount[a[i][j]] = -1;
    }
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      if (a[i][j] % 2 == 0)
      {
        occCount[a[i][j]]++;
      }
    }
  }

  int max = 0;
  for (const pair<int, int> & p : occCount)
  {
    if (occCount[p.first] > max)
    {
      bool edge = false;
      for (int i = 0; i < rows; i++)
      {
        if (a[i][0] == p.first || a[i][cols - 1] == p.first)
        {
          edge = true;
          break;
        }
      }

      for (int i = 0; i < cols; i++)
      {
        if (a[0][i] == p.first || a[rows - 1][i] == p.first)
        {
          edge = true;
          break;
        }
      }

      if (!edge)
      {
        max = occCount[p.first];
      }
    }
  }

  out << max + 1;
}

void PartTwo()
{
  ifstream in("day6.txt");
  ofstream out("out.txt");

  vector<int> xCoord;
  vector<int> yCoord;

  while (!in.eof())
  {
    char c;
    int x, y;
    in >> y;
    in >> c;
    in >> x;

    xCoord.push_back(x);
    yCoord.push_back(y);
  }

  int maxX = *std::max_element(xCoord.begin(), xCoord.end()) + 1;
  int maxY = *std::max_element(yCoord.begin(), yCoord.end()) + 1;

  int rows = maxX > maxY ? maxX : maxY;
  int cols = rows;

  int** a = new int*[rows];
  for (int i = 0; i < rows; i++)
    a[i] = new int[cols];

  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      a[i][j] = 0;

  in.clear();
  in.seekg(0);
  int nr = 1;

  vector<pair<int, int>> v;

  while (!in.eof())
  {
    char c;
    int x, y;
    in >> y;
    in >> c;
    in >> x;

    a[x][y] = nr;
    v.push_back(make_pair(x, y));
    nr += 2;
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      bool found = std::any_of(v.begin(), v.end(), [i, j](const pair<int, int> & p) {return p.first == i && p.second == j; });
      if (found)
        continue;

      int minDist = 100000;
      int count = 0;
      int x1 = 0, y1 = 0;
      for (pair<int, int> & p : v)
      {
        if (ManhattanDist(i, j, p.first, p.second) < minDist)
        {
          minDist = ManhattanDist(i, j, p.first, p.second);
          count = 1;
          x1 = p.first;
          y1 = p.second;
        }
        else if (ManhattanDist(i, j, p.first, p.second) == minDist)
        {
          count++;
        }
      }

      if (count == 1)
        a[i][j] = a[x1][y1] + 1;
      else
        a[i][j] = -1;
    }
  }

  map<int, int> occCount;
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      if (a[i][j] % 2 == 0)
        occCount[a[i][j]] = 0;
      else
        occCount[a[i][j]] = -1;
    }
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      if (a[i][j] % 2 == 0)
      {
        occCount[a[i][j]]++;
      }
    }
  }

  int max = 0;
  for (const pair<int, int> & p : occCount)
  {
    if (occCount[p.first] > max)
    {
      bool edge = false;
      for (int i = 0; i < rows; i++)
      {
        if (a[i][0] == p.first || a[i][cols - 1] == p.first)
        {
          edge = true;
          break;
        }
      }

      for (int i = 0; i < cols; i++)
      {
        if (a[0][i] == p.first || a[rows - 1][i] == p.first)
        {
          edge = true;
          break;
        }
      }

      if (!edge)
      {
        max = occCount[p.first];
      }
    }
  }

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      int sum = 0;

      for (const pair<int, int> & p : v)
      {
        sum += ManhattanDist(i, j, p.first, p.second);
      }

      if (sum < 10000)
      {
        v1.push_back(make_pair(i, j));
      }
    }
  }

  vector<pair<int, int>> visited;
  max = 0;
  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      bool found = std::any_of(v1.begin(), v1.end(), [i, j](const pair<int, int> & p) {return p.first == i && p.second == j; });
      if (!found)
        continue;

      found = std::any_of(visited.begin(), visited.end(), [i, j](const pair<int, int> & p) {return p.first == i && p.second == j; });
      if (found)
        continue;

      int sum = recursive_sum(visited, i, j, rows, cols);
      if (sum > max)
      {
        max = sum;
      }
    }
  }

  out << max;
}
