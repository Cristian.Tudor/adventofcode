#include <fstream>
#include <algorithm>
#include <string>
#include <vector>
using namespace std;

void PartOne();
void PartTwo();

enum Turn
{
  Left = 0,
  Straight,
  Right
};

struct Cart
{
  Cart(char** a, int x, int y)
    : a(a), x(x), y(y), c(a[x][y]), turn(Right), collided(false) 
  {
  }

  bool operator<(const Cart & cart) const
  {
    return (x < cart.x) || (x == cart.x && y < cart.y);
  }

  bool operator!=(const Cart & cart)
  {
    return (this != &cart);
  }

  void TurnCart()
  {
    if (turn == Left)
    {
      turn = Straight;
    }
    else if (turn == Straight)
    {
      turn = Right;

      if (c == 'v')
        c = '<';
      else if (c == '<')
        c = '^';
      else if (c == '>')
        c = 'v';
      else if (c == '^')
        c = '>';
    }
    else if (turn == Right)
    {
      turn = Left;

      if (c == 'v')
        c = '>';
      else if (c == '<')
        c = 'v';
      else if (c == '>')
        c = '^';
      else if (c == '^')
        c = '<';
    }
  }

  void UpdateCart()
  {
    if (c == '>')
    {
      if (a[x][y] == '-')
      {
        y++;
      }
      else if (a[x][y] == '\\')
      {
        c = 'v';
        x++;
      }
      else if (a[x][y] == '/')
      {
        c = '^';
        x--;
      }
      else if (a[x][y] == '+')
      {
        TurnCart();

        if (c == 'v')
          x++;
        else if (c == '>')
          y++;
        else if (c == '^')
          x--;
      }
    }
    else if (c == '<')
    {
      if (a[x][y] == '-')
      {
        y--;
      }
      else if (a[x][y] == '\\')
      {
        c = '^';
        x--;
      }
      else if (a[x][y] == '/')
      {
        c = 'v';
        x++;
      }
      else if (a[x][y] == '+')
      {
        TurnCart();

        if (c == 'v')
          x++;
        else if (c == '<')
          y--;
        else if (c == '^')
          x--;
      }
    }
    else if (c == '^')
    {
      if (a[x][y] == '|')
      {
        x--;
      }
      else if (a[x][y] == '\\')
      {
        c = '<';
        y--;
      }
      else if (a[x][y] == '/')
      {
        c = '>';
        y++;
      }
      else if (a[x][y] == '+')
      {
        TurnCart();

        if (c == '<')
          y--;
        else if (c == '^')
          x--;
        else if (c == '>')
          y++;
      }
    }
    else if (c == 'v')
    {
      if (a[x][y] == '|')
      {
        x++;
      }
      else if (a[x][y] == '\\')
      {
        c = '>';
        y++;
      }
      else if (a[x][y] == '/')
      {
        c = '<';
        y--;
      }
      else if (a[x][y] == '+')
      {
        TurnCart();

        if (c == '<')
          y--;
        else if (c == 'v')
          x++;
        else if (c == '>')
          y++;
      }
    }
  }

  char** a;
  int x;
  int y;
  char c;
  Turn turn;
  bool collided;
};

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day13.txt");
  ofstream out("out.txt");

  int sizeX = 0, sizeY = 0;

  while (!in.eof())
  {
    string s;
    std::getline(in, s);

    sizeY = s.size();
    sizeX++;
  }

  in.clear();
  in.seekg(0);

  char** a = new char*[sizeX];
  for (int i = 0; i < sizeX; i++)
    a[i] = new char[sizeY];

  int i = 0;
  while (!in.eof())
  {
    string s;
    std::getline(in, s);

    for (size_t j = 0; j < s.size(); j++)
      a[i][j] = s[j];

    i++;
  }

  vector<Cart> carts;

  for (int i = 0; i < sizeX; i++)
  {
    for (int j = 0; j < sizeY; j++)
    {
      if (a[i][j] == '>' || a[i][j] == '<')
      {
        carts.emplace_back(a, i, j);
        a[i][j] = '-';
      }
      else if (a[i][j] == '^' || a[i][j] == 'v')
      {
        carts.emplace_back(a, i, j);
        a[i][j] = '|';
      }
    }
  }

  bool found = false;
  while (!found)
  {
    std::sort(carts.begin(), carts.end());

    for (Cart & cart : carts)
    {
      cart.UpdateCart();

      for (Cart & cart1 : carts)
      {
        if (cart.x == cart1.x && cart.y == cart1.y && cart != cart1)
        {
          out << cart.y << "," << cart.x << endl;
          found = true;
          break;
        }
      }

      if (found)
        break;
    }
  }

  for (int i = 0; i < sizeX; ++i)
    delete[] a[i];
  delete[] a;
}

void PartTwo()
{
  ifstream in("day13.txt");
  ofstream out("out.txt");

  int sizeX = 0, sizeY = 0;

  while (!in.eof())
  {
    string s;
    std::getline(in, s);

    sizeY = s.size();
    sizeX++;
  }

  in.clear();
  in.seekg(0);

  char** a = new char*[sizeX];
  for (int i = 0; i < sizeX; i++)
    a[i] = new char[sizeY];

  int i = 0;
  while (!in.eof())
  {
    string s;
    std::getline(in, s);

    for (size_t j = 0; j < s.size(); j++)
      a[i][j] = s[j];

    i++;
  }

  vector<Cart> carts;

  for (int i = 0; i < sizeX; i++)
  {
    for (int j = 0; j < sizeY; j++)
    {
      if (a[i][j] == '>' || a[i][j] == '<')
      {
        carts.emplace_back(a, i, j);
        a[i][j] = '-';
      }
      else if (a[i][j] == '^' || a[i][j] == 'v')
      {
        carts.emplace_back(a, i, j);
        a[i][j] = '|';
      }
    }
  }

  while (true)
  {
    std::sort(carts.begin(), carts.end());

    for (Cart & cart : carts)
    {
      if (cart.collided)
        continue;

      cart.UpdateCart();

      for (Cart & cart1 : carts)
      {
        if (cart.x == cart1.x && cart.y == cart1.y && cart != cart1)
        {
          cart.collided = true;
          cart1.collided = true;
          break;
        }
      }
    }

    auto it = std::remove_if(carts.begin(), carts.end(), [](const Cart & cart) {return cart.collided; });
    if (it != carts.end())
    {
      carts.erase(it, carts.end());
      if (carts.size() == 1)
      {
        out << carts[0].y << "," << carts[0].x;
        break;
      }
    }
  }

  for (int i = 0; i < sizeX; ++i)
    delete[] a[i];
  delete[] a;
}
