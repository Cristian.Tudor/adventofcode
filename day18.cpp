#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <list>
using namespace std;

void PartOne();
void PartTwo();

#define ROWS 50
#define COLS 50

int AdjacentCount(char a[][COLS], int i, int j, char c)
{
  vector<pair<int, int>> pairs = { {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1} };
  int count = 0;

  for (const auto & x : pairs)
  {
    if (i + x.first >= 0 && i + x.first < ROWS && j + x.second >= 0 && j + x.second < COLS)
    {
      if (a[i + x.first][j + x.second] == c)
        count++;
    }
  }

  return count;
}

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day18.txt");
  ofstream out("out.txt");

  char a[ROWS][COLS];
  char b[ROWS][COLS];
  int l = 0;

  while (!in.eof())
  {
    for (int i = 0; i < COLS; i++)
    {
      char c;
      in >> c;
      a[l][i] = c;
    }

    string s;
    std::getline(in, s);

    l++;

    if (l == ROWS)
      break;
  }

  for (int k = 0; k < 10; k++)
  {
    for (int x = 0; x < ROWS; x++)
      for (int y = 0; y < COLS; y++)
        b[x][y] = a[x][y];

    for (int i = 0; i < ROWS; i++)
    {
      for (int j = 0; j < COLS; j++)
      {
        if (a[i][j] == '.')
        {
          if (AdjacentCount(a, i, j, '|') >= 3)
            b[i][j] = '|';
        }
        else if (a[i][j] == '|')
        {
          if (AdjacentCount(a, i, j, '#') >= 3)
            b[i][j] = '#';
        }
        else if (a[i][j] == '#')
        {
          if (AdjacentCount(a, i, j, '#') < 1 || AdjacentCount(a, i, j, '|') < 1)
            b[i][j] = '.';
        }
      }
    }

    for (int x = 0; x < ROWS; x++)
      for (int y = 0; y < COLS; y++)
        a[x][y] = b[x][y];
  }

  int lumberCount = 0;
  int treeCount = 0;

  for (int x = 0; x < ROWS; x++)
    for (int y = 0; y < COLS; y++)
      if (a[x][y] == '#')
        lumberCount++;
      else if (a[x][y] == '|')
        treeCount++;

  out << lumberCount * treeCount;
}

void PartTwo()
{
  ifstream in("day18.txt");
  ofstream out("out.txt");

  char a[ROWS][COLS];
  char b[ROWS][COLS];
  int l = 0;

  while (!in.eof())
  {
    for (int i = 0; i < COLS; i++)
    {
      char c;
      in >> c;
      a[l][i] = c;
    }

    string s;
    std::getline(in, s);

    l++;

    if (l == ROWS)
      break;
  }

  map<int, vector<int>> values;

  for (int k = 0; k < 1500; k++)
  {
    for (int x = 0; x < ROWS; x++)
      for (int y = 0; y < COLS; y++)
        b[x][y] = a[x][y];

    for (int i = 0; i < ROWS; i++)
    {
      for (int j = 0; j < COLS; j++)
      {
        if (a[i][j] == '.')
        {
          if (AdjacentCount(a, i, j, '|') >= 3)
            b[i][j] = '|';
        }
        else if (a[i][j] == '|')
        {
          if (AdjacentCount(a, i, j, '#') >= 3)
            b[i][j] = '#';
        }
        else if (a[i][j] == '#')
        {
          if (AdjacentCount(a, i, j, '#') < 1 || AdjacentCount(a, i, j, '|') < 1)
            b[i][j] = '.';
        }
      }
    }

    for (int x = 0; x < ROWS; x++)
      for (int y = 0; y < COLS; y++)
        a[x][y] = b[x][y];

    if (k > 1000)
    {
      int lumberCount = 0;
      int treeCount = 0;

      for (int x = 0; x < ROWS; x++)
        for (int y = 0; y < COLS; y++)
          if (a[x][y] == '#')
            lumberCount++;
          else if (a[x][y] == '|')
            treeCount++;

      values[lumberCount * treeCount].push_back(k);
    }
  }

  const auto & firstVal = *values.begin();
  int index = firstVal.second[1];
  int offset = firstVal.second[1] - firstVal.second[0];

  while (index < 1000000000)
  {
    index += offset;
  }

  int diff = index - 1000000000;

  for (const auto & val : values)
  {
    if (val.second[0] - firstVal.second[0] == offset - diff - 1)
    {
      out << val.first << endl;
      break;
    }
  }
}
