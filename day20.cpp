#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <set>
#include <queue>
#include <map>
#include <string>
using namespace std;

using Point = pair<int, int>;

map<Point, vector<Point>> doors;

void Connect(Point & p1, const Point & p2)
{
  doors[p1].push_back(p2);
  doors[p2].push_back(p1);
  p1 = p2;
}

void Parse(const string & regex, int & i, Point p)
{
  Point initialPoint = p;
  auto & [x, y] = p;

  while (true)
  {
    switch (regex[i++])
    {
    case 'N':
      Connect(p, { x, y - 1 });
      break;

    case 'E':
      Connect(p, { x + 1, y });
      break;

    case 'S':
      Connect(p, { x, y + 1 });
      break;

    case 'W':
      Connect(p, { x - 1, y });
      break;

    case '(':
      Parse(regex, i, p);
      break;

    case ')':
      return;

    case '|':
      p = initialPoint;
      break;

    case '$':
      return;
    }
  }
}

int main()
{
  ifstream in("day20.txt");
  ofstream out("out.txt");

  string regex;
  in >> regex;

  int i = 1;
  Parse(regex, i, { 0, 0 });

  set<Point> visited;
  queue<pair<Point, int>> q;

  q.push({ {0, 0}, 0 });
  int longest = 0, count = 0;

  while (!q.empty())
  {
    auto [n, length] = q.front();
    q.pop();

    if (visited.count(n))
      continue;

    if (length >= 1000)
      count++;

    visited.insert(n);

    longest = std::max(length, longest);
    for (const Point & p : doors[n])
      q.push({ p, length + 1 });
  }

  out << longest << endl;
  out << count << endl;

  return 0;
}
