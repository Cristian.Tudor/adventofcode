#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <iterator>
#include <map>
#include <set>
#include <list>
#include <array>
#include <assert.h>
using namespace std;

enum class Operation
{
  addr,
  addi,
  mulr,
  muli,
  banr,
  bani,
  borr,
  bori,
  setr,
  seti,
  gtir,
  gtri,
  gtrr,
  eqir,
  eqri,
  eqrr
};

struct Instruction
{
  Operation operation;
  array<int64_t, 3> data;
};

Operation GetOperation(const string & operationName)
{
  vector<string> operationNames { "addr", "addi", "mulr", "muli", "banr", "bani", "borr", "bori", "setr",
                                  "seti", "gtir", "gtri", "gtrr", "eqir", "eqri", "eqrr" };

  auto it = std::find(operationNames.begin(), operationNames.end(), operationName);
  assert(it != operationNames.end());

  return static_cast<Operation>(std::distance(operationNames.begin(), it));
}

void ExecuteOperation(array<int64_t, 6> & registers, const Instruction & instruction)
{
  auto & data = instruction.data;

  switch (instruction.operation)
  {
  case Operation::addr:
    registers[data[2]] = registers[data[0]] + registers[data[1]];
    break;

  case Operation::addi: 
    registers[data[2]] = registers[data[0]] + data[1]; 
    break;

  case Operation::mulr:
    registers[data[2]] = registers[data[0]] * registers[data[1]];
    break;

  case Operation::muli: 
    registers[data[2]] = registers[data[0]] * data[1]; 
    break;

  case Operation::banr:
    registers[data[2]] = registers[data[0]] & registers[data[1]];
    break;

  case Operation::bani: 
    registers[data[2]] = registers[data[0]] & data[1]; 
    break;

  case Operation::borr:
    registers[data[2]] = registers[data[0]] | registers[data[1]];
    break;

  case Operation::bori: 
    registers[data[2]] = registers[data[0]] | data[1]; 
    break;

  case Operation::setr: 
    registers[data[2]] = registers[data[0]]; 
    break;

  case Operation::seti: 
    registers[data[2]] = data[0]; 
    break;

  case Operation::gtir:
    registers[data[2]] = (data[0] > registers[data[1]] ? 1 : 0);
    break;

  case Operation::gtri:
    registers[data[2]] = (registers[data[0]] > data[1] ? 1 : 0);
    break;

  case Operation::gtrr:
    registers[data[2]] = (registers[data[0]] > registers[data[1]] ? 1 : 0);
    break;

  case Operation::eqir:
    registers[data[2]] = (data[0] == registers[data[1]] ? 1 : 0);
    break;

  case Operation::eqri:
    registers[data[2]] = (registers[data[0]] == data[1] ? 1 : 0);
    break;

  case Operation::eqrr:
    registers[data[2]] = (registers[data[0]] == registers[data[1]] ? 1 : 0);
    break;
  }
}

int main()
{
  ifstream in("day21.txt");
  ofstream out("out.txt");

  vector<Instruction> instructions;
  int ip = 0;

  string temp;
  in >> temp;
  in >> ip;
  std::getline(in, temp);

  while (!in.eof())
  {
    string opcode;
    in >> opcode;

    Instruction instr;
    instr.operation = GetOperation(opcode);

    in >> instr.data[0];
    in >> instr.data[1];
    in >> instr.data[2];

    instructions.push_back(instr);

    std::getline(in, temp);
  }

  set<int64_t> haltingValues;
  int64_t lastHaltingVal;

  array<int64_t, 6> registers;
  registers.fill(0);

  while (true)
  {
    ExecuteOperation(registers, instructions[registers[ip]]);
    ++registers[ip];

    if (registers[ip] == 28)
    {
      if (haltingValues.find(registers[5]) == haltingValues.end())
      {
        if (haltingValues.empty())
        {
          out << "Part 1: " << registers[5] << endl;
        }
        haltingValues.insert(registers[5]);
        lastHaltingVal = registers[5];
      }
      else
        break;
    }
  }

  out << "Part 2: " << lastHaltingVal << endl;
}
