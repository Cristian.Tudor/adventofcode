#include <algorithm>
#include <iterator>
#include <iostream>
#include <fstream>
#include <vector>
#include <numeric>
#include <string>
using namespace std;

enum class Team
{
  Immune,
  Infection
};

void Tokenize(const string & str, vector<string> & tokens, const string & sep = " ")
{
  size_t current, previous = 0;
  current = str.find_first_of(sep);

  while (current != string::npos)
  {
    tokens.push_back(str.substr(previous, current - previous));
    previous = current + 1;
    current = str.find_first_of(sep, previous);
  }

  tokens.push_back(str.substr(previous, current - previous));
}

vector<string> GetWeakImmune(const vector<string> & elements, const string & name)
{
  vector<string> result;

  auto element(std::find(elements.begin(), elements.end(), name));
  if (element != elements.end())
  {
    std::advance(element, 2);
    while (element->back() == ',')
    {
      result.push_back(element->substr(0, element->size() - 1));
      ++element;
    }
    result.push_back(*element);
  }

  return result;
}

struct Unit
{
  int count, hp, dmg, initiative;
  string attackType;
  vector<string> immune, weak;
  Team team;

  Unit(const string & line, const Team & team) : team(team)
  {
    vector<string> elements;

    Tokenize(line, elements, " ();");

    count = std::stoi(elements.at(0));
    hp = std::stoi(elements.at(4));

    auto element(std::find(elements.begin(), elements.end(), "does"));
    ++element;

    dmg = std::stoi(*element);
    ++element;

    attackType = *element;

    element = std::find(elements.begin(), elements.end(), "initiative");
    ++element;

    initiative = std::stoi(*element);

    weak = GetWeakImmune(elements, "weak");
    immune = GetWeakImmune(elements, "immune");
  }

  int GetPower() const 
  { 
    return count * dmg;
  }

  bool operator<(const Unit &unit) const
  {
    if (GetPower() < unit.GetPower())
      return true;

    if (GetPower() == unit.GetPower())
      return (initiative < unit.initiative);

    return false;
  }

  bool operator>(const Unit & unit) const { return unit < *this; }
};

bool IsFightFinished(const vector<Unit> & units)
{
  return std::find_if(units.begin(), units.end(), [](const Unit & unit) { return unit.team == Team::Immune; }) == units.end() || 
         std::find_if(units.begin(), units.end(), [](const Unit &unit)  { return unit.team == Team::Infection; }) == units.end();
}

vector<Unit> Fight(const vector<Unit> & initialUnits, const int & help)
{
  vector<Unit> units(initialUnits);

  for (auto & unit : units)
  {
    if (unit.team == Team::Immune)
      unit.dmg += help;
  }
  
  int round = 0;

  while (!IsFightFinished(units))
  {
    std::sort(units.begin(), units.end(), std::greater<Unit>());

    vector<pair<vector<Unit>::iterator, int>> attacks;

    for (auto & unit : units)
    {
      auto attacked(units.end());
      int attackMultiplier = 0;

      for (auto defender = units.begin(); defender != units.end(); ++defender)
      {
        if (defender->team != unit.team && std::find(defender->immune.begin(), defender->immune.end(), unit.attackType) == defender->immune.end()
                                        && std::find_if(attacks.begin(), attacks.end(), [&](const pair<vector<Unit>::iterator, int> & attack) 
        { return attack.first == defender; }) == attacks.end())
        {
          int unitMultiplier = 1;
          if (std::find(defender->weak.begin(), defender->weak.end(), unit.attackType) != defender->weak.end())
            unitMultiplier = 2;

          if (attacked == units.end() || (unitMultiplier > attackMultiplier) || (unitMultiplier == attackMultiplier && *defender > *attacked))
          {
            attacked = defender;
            attackMultiplier = unitMultiplier;
          }
        }
      }

      attacks.emplace_back(attacked, attackMultiplier);
    }

    vector<int> attackOrder(attacks.size());
    std::iota(attackOrder.begin(), attackOrder.end(), 0);
    std::sort(attackOrder.begin(), attackOrder.end(), [&](const size_t & index1, const size_t & index2)
    {
      return units[index1].initiative > units[index2].initiative;
    });

    bool anyUnitsKilled = false;

    for (auto & index : attackOrder)
    {
      if (attacks[index].first != units.end())
      {
        int totalDmg = units[index].GetPower() * attacks[index].second;
        int unitsKilled(totalDmg / (attacks[index].first->hp));

        attacks[index].first->count -= unitsKilled;
        if (attacks[index].first->count < 0)
          attacks[index].first->count = 0;

        anyUnitsKilled = anyUnitsKilled || (unitsKilled > 0);
      }
    }

    if (!anyUnitsKilled)
      break;

    std::vector<Unit> newUnits;
    for (auto & unit : units)
    {
      if (unit.count > 0)
        newUnits.push_back(unit);
    }
    std::swap(units, newUnits);

    ++round;
  }

  return units;
}

int main()
{
  ifstream in("day24.txt");

  string line;
  vector<Unit> units;

  std::getline(in, line);
  std::getline(in, line);
  while (!line.empty())
  {
    units.emplace_back(line, Team::Immune);
    std::getline(in, line);
  }

  std::getline(in, line);
  std::getline(in, line);
  while (!line.empty())
  {
    units.emplace_back(line, Team::Infection);
    std::getline(in, line);
  }

  int sum = 0;
  for (auto & unit : Fight(units, 0))
    sum += unit.count;

  cout << "Part 1: " << sum << endl;

  for (size_t help = 1; help < 10000; ++help)
  {
    auto fightResult = Fight(units, help);

    if (std::find_if(fightResult.begin(), fightResult.end(), [](const Unit & unit) { return unit.team == Team::Infection; }) == fightResult.end())
    {
      int sum = 0;
      for (auto & unit : fightResult)
        sum += unit.count;

      cout << "Part 2: " << sum << "\n";
      break;
    }
  }

  return 0;
}
