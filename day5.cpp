#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
using namespace std;

bool reacts(char a, char b);
void solve(string & str);
void PartOne();
void PartTwo();

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day5.txt");
  ofstream out("out.txt");

  string s;
  in >> s;

  solve(s);

  out << s.size();
}

void PartTwo()
{
  ifstream in("day5.txt");
  ofstream out("out.txt");

  string s;
  in >> s;

  vector<int> lenghts;

  for (char c = 'A'; c <= 'Z'; c++)
  {
    string tempStr = s;

    while (tempStr.find(c) != string::npos || tempStr.find(tolower(c)) != string::npos)
    {
      size_t pos = tempStr.find(c);
      if (pos == string::npos)
        pos = tempStr.find(tolower(c));

      if (pos != string::npos)
      {
        string s1;
        if (pos != 0)
          s1 = tempStr.substr(0, pos);

        string s2;
        if (pos + 1 < tempStr.size())
          s2 = tempStr.substr(pos + 1);

        tempStr = s1 + s2;
      }
    }

    if (tempStr == s)
      continue;

    solve(tempStr);

    lenghts.push_back(tempStr.size());
  }
     
  out << *std::min_element(lenghts.begin(), lenghts.end());
}

bool reacts(char a, char b)
{
  return (a != b && tolower(a) == tolower(b));
}

void solve(string & str)
{
  while (true)
  {
    bool foundReaction = false;

    for (size_t i = 0; i < str.size() - 1; i++)
    {
      if (reacts(str[i], str[i + 1]))
      {
        string s1 = str.substr(0, i);
        string s2 = str.substr(i + 2);
        str = s1 + s2;
        foundReaction = true;
        break;
      }
    }

    if (foundReaction == false)
      break;
  }
}
