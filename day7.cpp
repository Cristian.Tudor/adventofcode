#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <chrono>
#include <thread>
#include <set>
#include <map>
#include <cctype>
#include <assert.h>
using namespace std;

class Node;

int multiplyFactor = 50;

struct compare
{
  bool operator() (Node * node1, Node * node2);
};

class Node
{
public:
  Node(char aChar, const set<Node *, compare> & aPrev, const set<Node *, compare> & aNext)
  {
    c = aChar;
    prev = aPrev;
    next = aNext;
  }

  bool IsStarted() const
  {
    return started;
  }

  bool IsFinished() const
  {
    return finished;
  }

  void Update()
  {
    assert(started == true);

    tp2 = chrono::system_clock::now();
    if (chrono::duration_cast<chrono::milliseconds>(tp2 - tp1).count() > GetWorkTime())
      finished = true;
  }

  void StartWork()
  {
    started = true;
    tp1 = chrono::system_clock::now();
  }

  set<Node *, compare> & GetPrev() { return prev; }
  set<Node *, compare> & GetNext() { return next; }

  char GetChar() const { return c; }

private:
  char c;
  set<Node *, compare> prev;
  set<Node *, compare> next;
  chrono::system_clock::time_point tp1;
  chrono::system_clock::time_point tp2;
  bool started{ false };
  bool finished{ false };

  int GetWorkTime()
  {
    return (c - 4) * multiplyFactor;
  }
};

bool compare::operator()(Node * node1, Node * node2)
{
  return node1->GetChar() < node2->GetChar();
}

class Worker
{
public:
  Worker() = default;

  void StartWork(Node * node)
  {
    mNode = node;
    mNode->StartWork();
  }

  void Update()
  {
    if (mNode)
      mNode->Update();
  }

  bool IsIdle() const { return !mNode || !mNode->IsStarted() || mNode->IsFinished(); }

private:
  Node * mNode{ nullptr };
};

bool IsWorkRemaining(const vector<Node *> & nodes);

void PartOne();
void PartTwo();

int main()
{
  PartTwo();

  return 0;
}

bool IsWorkRemaining(vector<Node * > & nodes)
{
  return std::any_of(nodes.begin(), nodes.end(), [](Node * node) { return node->IsFinished() == false; });
}

void PartOne()
{
  ifstream in("day7.txt");
  ofstream out("out.txt");

  string result, startLetters;
  vector<string> v;

  while (!in.eof())
  {
    string temp;
    in >> temp;

    string s;
    in >> s;

    for (int i = 0; i < 5; i++)
      in >> temp;

    string s1;
    in >> s1;

    v.push_back(s + s1);

    std::getline(in, temp);
  }

  set<char> letters;
  for (const string & s : v)
  {
    letters.insert(s[0]);
    letters.insert(s[1]);
  }

  for (char c : letters)
  {
    if (std::none_of(v.begin(), v.end(), [c](const string & s) {return s[1] == c; }))
    {
      startLetters = startLetters + c;
    }
  }

  size_t index = 0;
  result = startLetters[index];

  while (!v.empty())
  {
    auto cmp = [](const string & str1, const string & str2) { return str1[1] < str2[1]; };
    set<string, decltype(cmp)> results(cmp);

    for (size_t i = 0; i < result.size(); i++)
    {
      for (const string & s : v)
      {
        if (s[0] == result[i])
          results.insert(s);
      }
    }

    if (index < startLetters.size() - 1)
      results.insert(string("-") + string(1, startLetters[index + 1]));

    bool found = false;
    for (const string & s : results)
    {
      bool isCompatible = std::any_of(v.begin(), v.end(), [&s, &result, &v](const string & str)
      {
        if (std::count(result.begin(), result.end(), s[1]) > 0)
          return false;

        char c = s[1];
        return std::none_of(v.begin(), v.end(), [c, &result](const string & s1) {return (c == s1[1] && result.find(s1[0]) == string::npos); });
      });

      if (isCompatible)
      {
        result += s[1];

        if (s[0] != '-')
        {
          auto it = std::find(v.begin(), v.end(), s);
          assert(it != v.end());
          v.erase(it);
        }
        else
          index++;

        found = true;
        break;
      }
    }

    if (!found)
    {
      index++;
      if (index < startLetters.size())
        result += startLetters[index];
      else
        break;
    }
  }

  out << result;
}

void PartTwo()
{
  ifstream in("day7.txt");
  ofstream out("out.txt");

  string startLetters;
  vector<string> v;
  vector<Node *> nodes;

  while (!in.eof())
  {
    string temp;
    in >> temp;

    string s;
    in >> s;

    for (int i = 0; i < 5; i++)
      in >> temp;

    string s1;
    in >> s1;

    v.push_back(s + s1);

    std::getline(in, temp);
  }

  set<char> letters;
  for (const string & s : v)
  {
    letters.insert(s[0]);
    letters.insert(s[1]);
  }

  int noPrev = 0;
  for (char c : letters)
  {
    if (std::none_of(v.begin(), v.end(), [c](const string & s) {return s[1] == c; }))
    {
      nodes.push_back(new Node(c, {}, {}));
      noPrev++;
    }
  }

  for (const string & s : v)
  {
    auto it = std::find_if(nodes.begin(), nodes.end(), [&s](Node * node) {return s[0] == node->GetChar(); });
    if (it == nodes.end())
      nodes.push_back(new Node(s[0], {}, {}));

    auto nextIt = std::find_if(nodes.begin(), nodes.end(), [&s](Node * node) {return s[1] == node->GetChar(); });
    if (nextIt == nodes.end())
      nodes.push_back(new Node(s[1], {}, {}));

    it = std::find_if(nodes.begin(), nodes.end(), [&s](Node * node) {return s[0] == node->GetChar(); });
    nextIt = std::find_if(nodes.begin(), nodes.end(), [&s](Node * node) {return s[1] == node->GetChar(); });

    (*it)->GetNext().insert(*nextIt);
    (*nextIt)->GetPrev().insert(*it);
  }

  Worker workers[5];
  int index = 0;

  chrono::system_clock::time_point tp1 = chrono::system_clock::now();

  while (IsWorkRemaining(nodes))
  {
    Worker * nextWorker{ nullptr };

    for (int i = 0; i < 5; i++)
    {
      if (workers[i].IsIdle() && !nextWorker)
        nextWorker = &workers[i];

      if (!workers[i].IsIdle())
        workers[i].Update();
    }

    if (nextWorker == nullptr)
      continue;

    Node * nextNode{ nullptr };

    if (index < noPrev)
    {
      nextNode = nodes[index];
      index++;
    }
    else
    {
      vector<Node *> nextNodes;

      for (Node * node : nodes)
      {
        if (node->IsStarted())
          continue;

        bool notFound = std::any_of(node->GetPrev().begin(), node->GetPrev().end(), [](Node * node1) { return node1->IsFinished() == false; });
        if (notFound)
          continue;

        nextNodes.push_back(node);
      }

      if (!nextNodes.empty())
        nextNode = *std::min_element(nextNodes.begin(), nextNodes.end(), [](Node * node1, Node * node2) { return node1->GetChar() < node2->GetChar(); });
    }

    if (nextNode)
      nextWorker->StartWork(nextNode);
  }

  chrono::system_clock::time_point tp2 = chrono::system_clock::now();

  long long duration = chrono::duration_cast<chrono::milliseconds>(tp2 - tp1).count() / multiplyFactor;
  out << duration;
}
