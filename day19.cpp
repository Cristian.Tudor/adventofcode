#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <iterator>
#include <map>
#include <set>
#include <list>
using namespace std;

void PartOne();
void PartTwo();

map<int, set<int>> opcodes;

struct Instruction
{
  Instruction() = default;

  Instruction(const string & opcode, int a, int b, int c) : opcode(opcode), a(a), b(b), c(c) {}

  string opcode;
  int a{ 0 };
  int b{ 0 };
  int c{ 0 };
};

struct RegistersState
{
  RegistersState() = default;

  RegistersState(int a, int b, int c, int d, int e, int f) : A{ a }, B{ b }, C{ c }, D{ d }, E{ e }, F{ f } {}

  int A{ 0 };
  int B{ 0 };
  int C{ 0 };
  int D{ 0 };
  int E{ 0 };
  int F{ 0 };
};

struct Sample
{
  Sample(const RegistersState & regState, const Instruction & instr, int currentIP, int regIP)
    : regState(regState), instruction(instr), currentIP(currentIP), regIP(regIP)
  {}

  RegistersState regState;
  Instruction instruction;
  int currentIP{ 0 };
  int regIP{ 0 };

  void Execute()
  {
    if (instruction.opcode == "addr")
    {
      SetReg(regIP, currentIP);
      SetReg(instruction.c, GetReg(instruction.a) + GetReg(instruction.b));
    }
    else if (instruction.opcode == "addi")
    {
      SetReg(regIP, currentIP);
      SetReg(instruction.c, GetReg(instruction.a) + instruction.b);
    }
    else if (instruction.opcode == "mulr")
    {
      SetReg(instruction.c, GetReg(instruction.a) * GetReg(instruction.b));
    }
    else if (instruction.opcode == "muli")
    {
      SetReg(instruction.c, GetReg(instruction.a) * instruction.b);
    }
    else if (instruction.opcode == "banr")
    {
      SetReg(instruction.c, GetReg(instruction.a) & GetReg(instruction.b));
    }
    else if (instruction.opcode == "bani")
    {
      SetReg(instruction.c, GetReg(instruction.a) & instruction.b);
    }
    else if (instruction.opcode == "borr")
    {
      SetReg(instruction.c, GetReg(instruction.a) | GetReg(instruction.b));
    }
    else if (instruction.opcode == "bori")
    {
      SetReg(instruction.c, GetReg(instruction.a) | instruction.b);
    }
    else if (instruction.opcode == "setr")
    {
      SetReg(regIP, currentIP);
      SetReg(instruction.c, GetReg(instruction.a));
    }
    else if (instruction.opcode == "seti")
    {
      SetReg(regIP, currentIP);
      SetReg(instruction.c, instruction.a);
    }
    else if (instruction.opcode == "gtir")
    {
      SetReg(instruction.c, instruction.a > GetReg(instruction.b) ? 1 : 0);
    }
    else if (instruction.opcode == "gtri")
    {
      SetReg(instruction.c, GetReg(instruction.a) > instruction.b ? 1 : 0);
    }
    else if (instruction.opcode == "gtrr")
    {
      SetReg(instruction.c, GetReg(instruction.a) > GetReg(instruction.b) ? 1 : 0);
    }
    else if (instruction.opcode == "eqir")
    {
      SetReg(instruction.c, instruction.a == GetReg(instruction.b) ? 1 : 0);
    }
    else if (instruction.opcode == "eqri")
    {
      SetReg(instruction.c, GetReg(instruction.a) == instruction.b ? 1 : 0);
    }
    else if (instruction.opcode == "eqrr")
    {
      SetReg(instruction.c, GetReg(instruction.a) == GetReg(instruction.b) ? 1 : 0);
    }

    SetReg(regIP, GetReg(regIP) + 1);
    currentIP = GetReg(regIP);
  }

  int GetReg(int i) const
  {
    if (i == 0)
      return regState.A;

    if (i == 1)
      return regState.B;

    if (i == 2)
      return regState.C;

    if (i == 3)
      return regState.D;

    if (i == 4)
      return regState.E;

    return regState.F;
  }

  void SetReg(int i, int val)
  {
    if (i == 0)
      regState.A = val;

    else if (i == 1)
      regState.B = val;

    else if (i == 2)
      regState.C = val;

    else if (i == 3)
      regState.D = val;

    else if (i == 4)
      regState.E = val;

    else 
      regState.F = val;
  }
};

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day19.txt");
  ofstream out("out.txt");

  vector<Instruction> instructions;
  int regIP = 0;
  int currentIP = 0;

  string temp;
  in >> temp;
  in >> regIP;
  std::getline(in, temp);

  while (!in.eof())
  {
    string opcode;
    in >> opcode;

    int a, b, c;
    in >> a;
    in >> b;
    in >> c;

    instructions.emplace_back(opcode, a, b, c);

    std::getline(in, temp);
  }

  RegistersState currentRegState;

  while (currentIP < instructions.size())
  {
    Sample currentSample(currentRegState, instructions[currentIP], currentIP, regIP);
    currentSample.Execute();

    currentRegState = currentSample.regState;
    currentIP = currentSample.currentIP;
  }

  out << currentRegState.A;
}

void PartTwo()
{
  ifstream in("day19.txt");
  ofstream out("out.txt");

  vector<Instruction> instructions;
  int regIP = 0;
  int currentIP = 0;

  string temp;
  in >> temp;
  in >> regIP;
  std::getline(in, temp);

  while (!in.eof())
  {
    string opcode;
    in >> opcode;

    int a, b, c;
    in >> a;
    in >> b;
    in >> c;

    instructions.emplace_back(opcode, a, b, c);

    std::getline(in, temp);
  }

  int val = 0;
  long long count = 0;
  RegistersState currentRegState;
  currentRegState.A = 1;

  while (currentIP < instructions.size())
  {
    Sample currentSample(currentRegState, instructions[currentIP], currentIP, regIP);
    currentSample.Execute();

    currentRegState = currentSample.regState;
    currentIP = currentSample.currentIP;

    if (count == 20)
      currentRegState.F = 1000;

    if (count % 50000 == 0)
    {
      out << currentRegState.A << " " << currentRegState.B << " " << currentRegState.C << " " << currentRegState.D << " " << currentRegState.E << " " << currentRegState.F << endl;
    }

    if (count++ == 1000000000)
      break;
  }
}
