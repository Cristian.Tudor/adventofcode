#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <list>
using namespace std;

void PartOne();
void PartTwo();

int GetNextPos(const vector<int> & recipes, int pos, int offset)
{
  return (pos + offset) % recipes.size();
}

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day14.txt");
  ofstream out("out.txt");

  vector<int> recipes{ 3, 7 };

  size_t recipeCount = 0;
  in >> recipeCount;

  int pos1 = 0, pos2 = 1;

  while (true)
  {
    int newRecipe = recipes[pos1] + recipes[pos2];
    if (newRecipe < 10)
    {
      recipes.push_back(newRecipe);
    }
    else
    {
      recipes.push_back(newRecipe / 10);
      recipes.push_back(newRecipe % 10);
    }

    pos1 = GetNextPos(recipes, pos1, recipes[pos1] + 1);
    pos2 = GetNextPos(recipes, pos2, recipes[pos2] + 1);

    if (recipes.size() >= recipeCount + 10)
    {
      for (size_t i = recipeCount; i < recipeCount + 10; i++)
        out << recipes[i];

      break;
    }
  }
}

void PartTwo()
{
  ifstream in("day14.txt");
  ofstream out("out.txt");

  vector<int> recipes{ 3, 7 };

  size_t recipeCount = 0;
  in >> recipeCount;
  recipeCount *= 100;

  int pos1 = 0, pos2 = 1;

  while (true)
  {
    int newRecipe = recipes[pos1] + recipes[pos2];
    if (newRecipe < 10)
    {
      recipes.push_back(newRecipe);
    }
    else
    {
      recipes.push_back(newRecipe / 10);
      recipes.push_back(newRecipe % 10);
    }

    pos1 = GetNextPos(recipes, pos1, recipes[pos1] + 1);
    pos2 = GetNextPos(recipes, pos2, recipes[pos2] + 1);

    if (recipes.size() >= recipeCount)
      break;
  }

  recipeCount /= 100;

  vector<int> v;
  while (recipeCount)
  {
    v.insert(v.begin(), recipeCount % 10);
    recipeCount /= 10;
  }

  bool found = false;
  while (!found)
  {
    for (size_t i = 0; i < recipes.size() - 6; i++)
    {
      if (std::equal(recipes.begin() + i, recipes.begin() + i + 6, v.begin()))
      {
        out << i;
        found = true;
        break;
      }
    }
  }
}
