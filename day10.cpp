#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

int ManhattanDist(const pair<int, int> & pos1, const pair<int, int> & pos2)
{
  return abs(pos1.first - pos2.first) + abs(pos1.second - pos2.second);
}

int main()
{
  ifstream in("day10.txt");
  ofstream out("out.txt");

  vector<pair<int, int>> positions;
  vector<pair<int, int>> velocities;

  int minPos = 100000;
  int offset = 2000;

  while (!in.eof())
  {
    char c;
    for (int i = 0; i < 10; i++)
      in >> c;

    int y;
    in >> y;

    in >> c;

    int x;
    in >> x;

    for (int i = 0; i < 11; i++)
      in >> c;

    int velY;
    in >> velY;

    in >> c;

    int velX;
    in >> velX;

    in >> c;

    positions.push_back(make_pair(x, y));
    velocities.push_back(make_pair(velX, velY));

    if (x < minPos)
      minPos = x;

    if (y < minPos)
      minPos = y;
  }

  int offsetPos = offset - minPos;
  int maxPos = 0;

  for (auto & pos : positions)
  {
    pos.first += offsetPos;
    pos.second += offsetPos;

    if (pos.first > maxPos)
      maxPos = pos.first;

    if (pos.second > maxPos)
      maxPos = pos.second;
  }

  int rows = maxPos + offset;
  int cols = rows;

  char** a = new char*[rows];
  for (int i = 0; i < rows; i++)
    a[i] = new char[cols];

  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      a[i][j] = '.';

  for (const auto & pos : positions)
    a[pos.first][pos.second] = '#';

  int minDist = INT_MAX;
  size_t index = 0, x1 = 0, y1 = 0, x2 = 0, y2 = 0, pos1 = 0, pos2 = 0;

  for (size_t k = 0; k < 10159; k++)
  {
    vector<pair<int, int>> placedNow;

    for (size_t i = 0; i < positions.size(); i++)
    {
      int posX = positions[i].first;
      int posY = positions[i].second;

      if (none_of(placedNow.begin(), placedNow.end(), [posX, posY](const pair<int, int> & pos) { return pos.first == posX && pos.second == posY; }))
        a[positions[i].first][positions[i].second] = '.';

      positions[i].first += velocities[i].first;
      positions[i].second += velocities[i].second;

      placedNow.push_back(make_pair(positions[i].first, positions[i].second));

      a[positions[i].first][positions[i].second] = '#';
    }

    int maxDist = 0;

    for (size_t i = 0; i < positions.size() - 1; i++)
    {
      for (size_t j = i + 1; j < positions.size(); j++)
      {
        if (ManhattanDist(positions[i], positions[j]) > maxDist)
        {
          maxDist = ManhattanDist(positions[i], positions[j]);
          pos1 = i;
          pos2 = j;
        }
      }
    }

    if (maxDist < minDist)
    {
      minDist = maxDist;
      index = k;
      x1 = positions[pos1].first;
      x2 = positions[pos2].first;
      y1 = positions[pos1].second;
      y2 = positions[pos2].second;
    }
  }

  out << index;
  out << endl;

  for (size_t i = std::min(x1, x2) - 70; i < std::max(x1, x2) + 70; i++)
  {
    for (size_t j = std::min(y1, y2) - 70; j < std::max(y1, y2) + 70; j++)
      out << a[i][j] << " ";

    out << endl;
  }

  for (int i = 0; i < rows; ++i)
    delete[] a[i];
  delete[] a;

  return 0;
}
