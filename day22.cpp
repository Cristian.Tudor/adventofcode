#include <vector>
#include <iostream>
#include <queue>
#include <tuple>
#include <set>
using namespace std;

enum Region
{
  Rocky = 0,
  Wet,
  Narrow
};

enum Tool
{
  Torch = 0,
  Gear,
  None
};

using RowColTool = tuple<int, int, int>;
using CostRowColTool = tuple<int, int, int, int>;

bool IsValid(Region region, Tool tool) 
{
  if (region == Rocky && (tool == Torch || tool == Gear))
    return true;

  if (region == Wet && (tool == Gear || tool == None))
    return true;

  if (region == Narrow && (tool == Torch || tool == None))
    return true;

  return false;
}

int main() 
{
  int rows = 2000, cols = 1000;
  int depth = 3339, targetX = 715, targetY = 10;

  vector<vector<int>> geologicIndices(rows, vector<int>(cols, 0));
  vector<vector<int>> erosionLevels(rows, vector<int>(cols, 0));

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      if (i == 0 && j == 0)
        geologicIndices[i][j] = 0;
      else if (i == targetX && j == targetY)
        geologicIndices[i][j] = 0;
      else if (i == 0)
        geologicIndices[i][j] = j * 16807;
      else if (j == 0)
        geologicIndices[i][j] = i * 48271;
      else
        geologicIndices[i][j] = erosionLevels[i - 1][j] * erosionLevels[i][j - 1];

      erosionLevels[i][j] = (geologicIndices[i][j] + depth) % 20183;
    }
  }

  int risk = 0;
  for (int i = 0; i <= targetX; i++) 
  {
    for (int j = 0; j <= targetY; j++) 
      risk += (erosionLevels[i][j] % 3);
  }

  cout << risk << endl;

  vector<int> dirX{-1, 0, 1, 0};
  vector<int> dirY{0, 1, 0, -1};

  set<RowColTool> visited;
  priority_queue<CostRowColTool> q;

  q.push({ 0, 0, 0, 0 });

  while (!q.empty()) 
  {
    int cost, row, col, tool;
    std::tie(cost, row, col, tool) = q.top(); 
    q.pop();

    cost = -cost;

    if (row == targetX && col == targetY && tool == Torch) 
    {
      cout << cost << endl;
      break;
    }

    if (visited.count(RowColTool(row, col, tool)) == 1)
      continue;

    visited.insert(RowColTool(row, col, tool));

    for (int t = 0; t < 3; t++) 
    {
      if (IsValid((Region) (erosionLevels[row][col] % 3), (Tool) t)) 
      {
        q.push(CostRowColTool(-(cost + 7), row, col, t));
      }
    }

    for (int dir = 0; dir < 4; dir++) 
    {
      int x = row + dirX[dir];
      int y = col + dirY[dir];

      if (!(x >= 0 && x < rows && y >= 0 && y < cols))
        continue;

      if (IsValid((Region)(erosionLevels[x][y] % 3), (Tool) tool)) 
      {
        q.push(CostRowColTool(-(cost + 1), x, y, tool));
      }
    }
  }
}
