#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cctype>
using namespace std;

void PartOne();
void PartTwo();

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day12.txt");
  ofstream out("out.txt");

  vector<pair<string, char>> notes;

  string temp;
  in >> temp;
  in >> temp;

  string initialState, currentState;
  in >> initialState;

  int offsetLeft = 5;
  int offsetRight = 30;
  string offsetStrLeft(offsetLeft, '.');
  string offsetStrRight(offsetRight, '.');
  currentState = offsetStrLeft + initialState + offsetStrRight;

  while (!in.eof())
  {
    string note;
    in >> note;

    in >> temp;

    char result;
    in >> result;

    notes.push_back(make_pair(note, result));

    std::getline(in, temp);
  }

  size_t generationCount = 20;
  for (size_t i = 0; i < generationCount; i++)
  {
    string newState(currentState.size(), '.');

    for (size_t j = 0; j < newState.size() - 5; j++)
    {
      string str(currentState.substr(j, 5));

      for (const auto & note : notes)
      {
        if (str == note.first) 
        {
          newState[j + 2] = note.second;
          break;
        }
      }
    }

    currentState = newState;
  }

  int sum = 0;
  for (size_t i = 0; i < currentState.size(); i++)
  {
    if (currentState[i] == '#')
      sum += i - offsetLeft;
  }

  out << sum;
}

void PartTwo()
{
  ifstream in("day12.txt");
  ofstream out("out.txt");

  vector<pair<string, char>> notes;

  string temp;
  in >> temp;
  in >> temp;

  string initialState, currentState;
  in >> initialState;

  int offsetLeft = 5;
  int offsetRight = 150;
  string offsetStrLeft(offsetLeft, '.');
  string offsetStrRight(offsetRight, '.');
  currentState = offsetStrLeft + initialState + offsetStrRight;

  while (!in.eof())
  {
    string note;
    in >> note;

    in >> temp;

    char result;
    in >> result;

    notes.push_back(make_pair(note, result));

    std::getline(in, temp);
  }

  long long generationCount = 50000000000;
  int currentSum = 0, previousSum = 0, diff = 0;

  for (size_t i = 0; i < generationCount; i++)
  {
    string newState(currentState.size(), '.');

    for (size_t j = 0; j < newState.size() - 5; j++)
    {
      string str(currentState.substr(j, 5));

      for (const auto & note : notes)
      {
        if (str == note.first)
        {
          newState[j + 2] = note.second;
          break;
        }
      }
    }

    int sum = 0;
    for (size_t i = 0; i < currentState.size(); i++)
    {
      if (currentState[i] == '#')
        sum += i - offsetLeft;
    }

    currentSum = sum;

    if (diff == currentSum - previousSum)
    {
      out << currentSum + (generationCount - i) * diff;
      break;
    }
    else
    {
      diff = currentSum - previousSum;
    }

    previousSum = currentSum;

    currentState = newState;
  }
}
