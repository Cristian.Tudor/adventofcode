#include <fstream>
#include <numeric>
#include <algorithm>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <list>
using namespace std;

char ** a;
int rows = 0, cols = 0, globalCount = 0, val = 0, counter = 0, stillWaterCount = 0;

set<pair<int, int>> visited;
vector<int> colsToExit;
vector<int> basePoints;

void Search(int i, int j);

int main()
{
  ifstream in("day17.txt");
  ofstream out("out.txt");

  int maxX = 0, maxY = 0;

  while (!in.eof())
  {
    char t, c;
    in >> t;
    in >> c;

    int v1;
    in >> v1;

    in >> c;
    in >> c;
    in >> c;

    int v2, v3;
    in >> v2;
    in >> c;
    in >> c;
    in >> v3;

    string s;
    std::getline(in, s);

    if (t == 'x')
    {
      if (v1 > maxY)
        maxY = v1;
    }
    else if (t == 'y')
    {
      if (v1 > maxX)
        maxX = v1;
    }
  }

  rows = maxX + 1;
  cols = maxY + 1000;

  a = new char*[rows];
  for (int i = 0; i < rows; i++)
    a[i] = new char[cols];

  for (int i = 0; i < rows; i++)
    for (int j = 0; j < cols; j++)
      a[i][j] = '.';

  in.clear();
  in.seekg(0);

  while (!in.eof())
  {
    char t, c;
    in >> t;
    in >> c;

    int v1;
    in >> v1;

    in >> c;
    in >> c;
    in >> c;

    int v2, v3;
    in >> v2;
    in >> c;
    in >> c;
    in >> v3;

    string s;
    std::getline(in, s);

    if (t == 'x')
    {
      for (int i = v2; i <= v3; i++)
        a[i][v1] = '#';
    }
    else if (t == 'y')
    {
      for (int i = v2; i <= v3; i++)
        a[v1][i] = '#';
    }
  }

  int minClayPos = 0;
  bool foundClayPos = false;

  for (int i = 0; i < rows; i++)
  {
    for (int j = 0; j < cols; j++)
    {
      if (a[i][j] == '#')
      {
        minClayPos = i - 1;
        foundClayPos = true;
        break;
      }
    }

    if (foundClayPos)
      break;
  }

  a[0][500] = '+';

  Search(0, 500);

  globalCount += minClayPos;

  out << visited.size() - globalCount << endl;
  out << stillWaterCount << endl << endl;

  for (const auto & item : visited)
    a[item.first][item.second] = 'x';

  for (int i = 0; i < rows; i++)
  {
    for (int j = 350; j < 700; j++)
      out << a[i][j] << " ";

    out << endl;
  }

  return 0;
}

bool IsNotAdded(int i, int j)
{
  return std::none_of(visited.begin(), visited.end(), [i, j](const pair<int, int> & pos) {return pos.first == i && pos.second == j; });
}

int SearchLeft(int i, int j, int & bound)
{
  bool isSand = (j > 0 && a[i][j - 1] == '.');
  bool isFalling = (i < rows - 1) && a[i + 1][j - 1] == '.' && IsNotAdded(i + 1, j - 1);

  if (isSand && !isFalling)
  {
    visited.insert(make_pair(i, j - 1));
    return SearchLeft(i, j - 1, bound);
  }

  if (isSand) // falling
    return j - 1;

  bound = j;

  return -1;
}

int SearchRight(int i, int j, int & bound)
{
  bool isSand = (j < cols - 1 && a[i][j + 1] == '.');
  bool isFalling = (i < rows - 1) && a[i + 1][j + 1] == '.' && IsNotAdded(i + 1, j + 1);

  if (isSand && !isFalling)
  {
    visited.insert(make_pair(i, j + 1));
    return SearchRight(i, j + 1, bound);
  }

  if (isSand) // falling
    return j + 1;

  bound = j;

  return -1;
}

void SearchRow(int i, int j)
{
  int leftBound = 0, rightBound = 0;
  int leftIndex = SearchLeft(i, j, leftBound);
  int rightIndex = SearchRight(i, j, rightBound);

  if (leftIndex == -1 && rightIndex == -1)
  {
    stillWaterCount += rightBound - leftBound + 1;
    visited.insert(make_pair(i, j));
    Search(i - 1, j);
  }
  else
  {
    if (leftIndex != -1)
    {
      Search(i - 1, leftIndex);
    }

    if (rightIndex != -1)
    {
      Search(i - 1, rightIndex);
    }
  }
}

void Search(int i, int j)
{
  if (i == rows - 1)
  {
    colsToExit.push_back(j);
    return;
  }

  if (std::any_of(colsToExit.begin(), colsToExit.end(), [j](int col) { return col == j; }))
    return;

  if (a[i + 1][j] == '.')
  {
    if (IsNotAdded(i + 1, j))
    {
      visited.insert(make_pair(i + 1, j));
      Search(i + 1, j);
    }
    else
    {
      if (val == 0)
        val = i;

      if (abs(i - val) != 1)
      {
        basePoints.push_back(val);
        counter = 0;
      }
      else
        counter++;

      val = i;

      bool isValid = std::none_of(basePoints.begin(), basePoints.end(), [i](int point) {return (counter == 0) && (point == i + 1); });
      if (isValid)
      {
        visited.insert(make_pair(i, j));
        SearchRow(i, j);
      }
    }
  }
  else
  {
    if (i < rows - 2 && a[i + 2][j] == '#')
    {
      int rowCount = 0, colCount1 = 1, colCount2 = 1, k = i + 1, l = j, count1 = 0, count2 = 0;
      while (a[k++][j] == '#')
        rowCount++;

      while (a[i + 1][l--] == '#')
        count1++;

      l = j;
      while (a[i + 1][l++] == '#')
        count2++;

      if (count1 == 1 && count2 == 1)
      {
        k = j;
        if (a[i + 1][k + 1] == '.')
        {
          while (a[i + 1][++k] == '.')
            colCount1++;

          colCount1++;

          if (a[i][k] == '.') // go right
          {
            for (int k = i + 1; k < i + 1 + rowCount; k++)
              for (int l = j; l < j + colCount1 + 1; l++)
                a[k][l] = '.';

            globalCount += rowCount * 2 + colCount1 - 2;
          }
        }

        k = j;
        if (a[i + 1][k - 1] == '.')
        {
          while (a[i + 1][--k] == '.')
            colCount2++;

          colCount2++;

          if (a[i][k] == '.') // go left
          {
            for (int k = i + 1; k < i + 1 + rowCount; k++)
              for (int l = j - colCount2; l <= j; l++)
                a[k][l] = '.';

            globalCount += rowCount * 2 + colCount2 - 2;
          }
        }
      }
      else
      {
        if (count1 > count2)
        {
          globalCount += rowCount * count1;

          for (int k = i + 1; k < i + 1 + rowCount; k++)
            for (int l = j - count1; l <= j; l++)
              a[k][l] = '.';
        }
        else
        {
          globalCount += rowCount * count2;

          for (int k = i + 1; k < i + 1 + rowCount; k++)
            for (int l = j; l < j + count2 + 1; l++)
              a[k][l] = '.';
        }
      }

      visited.insert(make_pair(i + 1, j));
      Search(i + 1, j);
    }
    else
    {
      SearchRow(i, j);
    }
  }
}
