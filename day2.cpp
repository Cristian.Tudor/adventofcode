#include <fstream>
#include <string>
#include <vector>
#include <assert.h>
using namespace std;

void PartOne();
void PartTwo();

int main()
{
  PartTwo();

  return 0;
}

void PartOne()
{
  ifstream in("day2.txt");
  ofstream out("out.txt");

  int twoOccCount = 0, threeOccCount = 0;

  while (!in.eof())
  {
    string s;
    in >> s;

    for (auto c : s)
    {
      if (std::count(s.begin(), s.end(), c) == 2)
      {
        twoOccCount++;
        break;
      }
    }

    for (auto c : s)
    {
      if (std::count(s.begin(), s.end(), c) == 3)
      {
        threeOccCount++;
        break;
      }
    }
  }

  out << twoOccCount * threeOccCount;
}

void PartTwo()
{
  ifstream in("day2.txt");
  ofstream out("out.txt");

  vector<string> v;

  while (!in.eof())
  {
    string s;
    in >> s;

    v.push_back(s);
  }

  for (size_t i = 0; i < v.size() - 1; i++)
  {
    for (size_t j = i + 1; j < v.size(); j++)
    {
      assert(v[i].size() == v[j].size());

      int count = 0;
      int pos = 0;

      for (size_t k = 0; k < v[i].size(); k++)
      {
        if (v[i][k] != v[j][k])
        {
          count++;
          pos = k;
        }
      }

      if (count == 1)
      {
        string s1 = v[i].substr(0, pos);
        string s2 = v[i].substr(pos + 1);

        out << s1 + s2;
      }
    }
  }
}